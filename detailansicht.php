<?php
use \php_classes\cls_infoServer;

// Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	//	$path = str_replace ( "err/", "", $path );
	
	if (file_exists ( $path )) {
		require $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );

$o_server = new cls_infoServer();
$s_pathPre = $o_server->getS_path();
?>
<!doctype html>
<html lang="de" class="h-100">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<title>Detailansicht</title>

<meta http-equiv="X-UA-Compatible" content="IE=10" />

<link href="<?php // echo $s_pathPre; ?>library/bootstrap-4.5.2-dist/css/bootstrap.css" rel="stylesheet">
<link href="<?php // echo $s_pathPre; ?>library/bootstrap-4.5.2-dist/css/sticky-footer-navbar.css" rel="stylesheet">
<link href="<?php // echo $s_pathPre; ?>css/own_Bootstrap.css" rel="stylesheet">

<link rel="shortcut icon" type="image/x-icon" href="<?php //echo $s_pathPre; ?>img/favicon-32x32-keiml.png">
<link href="<?php // echo $s_pathPre; ?>css/site.css" rel="stylesheet">
<link href="<?php // echo $s_pathPre; ?>css/own_design.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="<?php //echo $s_pathPre; ?>library/dataTables/css/jquery.dataTables.css" />
<link rel="stylesheet" type="text/css" href="<?php //echo $s_pathPre; ?>library/dataTables/css/dataTables.bootstrap4.css" />
<link rel="stylesheet" type="text/css" href="<?php //echo $s_pathPre; ?>library/datepicker/bootstrap-datepicker.css" />

<link href="<?php // echo $s_pathPre; ?>css/own_DataTables.css" rel="stylesheet">
<link href="<?php // echo $s_pathPre; ?>library/dataTables/css/responsive.bootstrap4.min.css" rel="stylesheet">

</head>

<body class="d-flex flex-column h-100">
	<header>
		<!-- Fixed navbar -->
		<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
			<a class="navbar-brand" id="headline" href="#">SVLFG - Überwachung von Auslieferungsbeschreibungen</a> 
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="navbar-brand" href="index.php">Zurück zur Startseite</a>
				</li>
			</ul>
		</nav>
	</header>
	<main role="main" class="flex-shrink-0">
		<div class="body_container">
			<h1 class="mt-5 text-center">Detail-Ansicht vom Änderungsantrag:</h1>

		</div>
		<h2 id="AEA_nr"><?php echo $_GET["nr"]?></h2>
		<h3>vom Team</h3>
		<h2 id="TEAM_name"><?php echo $_GET["teamname"]?></h2>

		<br>
		<div class="table_container row">
			<div class="date_container col-sm" >

				<table id="detail_data" class="display" style="width: 100%">
					<thead>
						<tr>
							<th>zu Erledigen bis:</th>
							<th>Abgeschlossen am:</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="comment_container col-sm-6">
				<table id="comment_data" class="display" style="width: 100%">
					<thead>
						<tr>
							<th>Kommentar</th>
							<th>Zu erledigen</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="mail_container col-sm">
				<table id="mail_data" class="display" style="width: 50%">
					<thead>
						<tr>
							<th>Mail-Erinnerung gesendet am:</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="to_dashboard">
	
			<button class="button_special" id="btndates">
				<b>Termin / Erledigt bearbeiten</b>
			</button>
			<button class="button_special" id="btncomment">
				<b>Kommentar bearbeiten / hinzufügen</b>
			</button>
			<button class="button_special" id="btnmaildate">
				<b>Mail-Erinnerung bearbeiten / hinzufügen</b>
			</button>
		</div>
		<div id="popover" class="popover">
			
		</div> 		
	    <div class="modal fade"  tabindex="-1" role="dialog"  aria-hidden="true" id="modal">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="title"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" id="modal_body"> 	 
					</div>
					<div class="modal-footer justify-content-center" id="modal_footer">
		
					</div>
				</div>
			</div>
		</div> 		
	</main>
	<footer class="footer mt-auto py-3">
		<div class="container  text-center">
			<span class="text-container">SVLFG - Überwachung von
				Auslieferungsbeschreibungen</span>
		</div>
	</footer>
	<script src="<?php //echo $s_pathPre; ?>library/jquery/jquery-3.5.1.min.js"></script>
	<script src="<?php //echo $s_pathPre; ?>library/bootstrap-4.5.2-dist/js/bootstrap.bundle.min.js"></script>
	<script src="<?php //echo $s_pathPre; ?>library/dataTables/js/jquery.dataTables.min.js"></script>
	<script src="<?php //echo $s_pathPre; ?>library/dataTables/js/dataTables.bootstrap4.js"></script>
	<script src="<?php //echo $s_pathPre; ?>library/datepicker/bootstrap-datepicker.js"></script>
	<script src="<?php //echo $s_pathPre; ?>library/datepicker/bootstrap-datepicker.de.min.js"></script>
	<script src="<?php //echo $s_pathPre; ?>library/dataTables/js/dataTables.responsive.min.js"></script>
	<script src="<?php //echo $s_pathPre; ?>library/dataTables/js/responsive.bootstrap4.min.js"></script>

	<script src="js/ajax/ajax_select/ajaxCall_detailansicht.js"></script>	
	<script src="js/ajax/ajax_insert/ajax_insert_detail_comment.js"></script>	
	<script src="js/ajax/ajax_insert/ajax_insert_detail_maildate.js"></script>
	<script src="js/ajax/ajax_update/ajax_update_detail_comment.js"></script>
	<script src="js/ajax/ajax_update/ajax_update_detail_dates.js"></script>
	<script src="js/ajax/ajax_update/ajax_update_detail_maildate.js"></script>
		
	<script src="js/plausibility.js"></script>
	<script src="js/popover_infos.js"></script>
	<script src="js/popover_close.js"></script>	
	<script src="js/toggle_class_selected.js"></script>

	<script src="js/modal/detail/datepicker_detail.js"></script>
	<script src="js/modal/detail/modal_newentry_detail_comment.js"></script>
	<script src="js/modal/detail/modal_newentry_detail_maildate.js"></script>
	<script src="js/modal/detail/modal_update_detail_comment.js"></script>
	<script src="js/modal/detail/modal_update_detail_dates.js"></script>
	<script src="js/modal/detail/modal_update_detail_maildate.js"></script>

	</body>
</html>