<?php
use php_classes\cls_infoServer;

// Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	//	$path = str_replace ( "err/", "", $path );
	
	if (file_exists ( $path )) {
		require_once $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );

$o_server = new cls_infoServer();
$s_pathPre = $o_server->getS_path();
?>
<!doctype html>
<html lang="de" class="h-100">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<title>Dashboard</title>

<meta http-equiv="X-UA-Compatible" content="IE=10" />

<link href="<?php  //echo $s_pathPre; ?>library/bootstrap-4.5.2-dist/css/bootstrap.css" rel="stylesheet">
<link href="<?php  //echo $s_pathPre; ?>library/bootstrap-4.5.2-dist/css/sticky-footer-navbar.css" rel="stylesheet">
<link href="<?php  //echo $s_pathPre; ?>css/own_Bootstrap.css" rel="stylesheet">

<link rel="shortcut icon" type="image/x-icon" href="<?php //echo $s_pathPre; ?>img/favicon-32x32-keiml.png">
<link href="<?php  //echo $s_pathPre; ?>css/site.css" rel="stylesheet">
<link href="<?php  //echo $s_pathPre; ?>css/own_design.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="<?php //echo $s_pathPre; ?>library/dataTables/css/jquery.dataTables.css" />
<link rel="stylesheet" type="text/css" href="<?php //echo $s_pathPre; ?>library/dataTables/css/dataTables.bootstrap4.css" />
<link rel="stylesheet" type="text/css" href="<?php //echo $s_pathPre; ?>library/datepicker/bootstrap-datepicker.css" />

<link href="<?php //echo $s_pathPre; ?>css/own_DataTables_dashboard.css" rel="stylesheet">
</head>

	<body class="d-flex flex-column h-100">
	<header>
		<!-- Fixed navbar -->
		<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
			<a class="navbar-brand" id="headline" href="#">SVLFG - Überwachung von Auslieferungsbeschreibungen</a>
		</nav>
	</header>
	<main role="main" class="flex-shrink-0">
		<div class="body_container">
			<h1 class="mt-5 text-center">Dashboard</h1>
		</div>
		<div class="view_container">

			<h2>Aktuell sind noch</h2>
			<h2 id="counter_number">(Platzhalter für Zähler)</h2>
			<h2>Anträge offen:</h2>
			<br>
			<h3 id="description_text">Wenn Sie auf eine Antragsnummer klicken, gelangen Sie zur Antragsansicht.</h3>
			<div class="row text-center" style="overflow-x: auto;">		 
				<div class="table_container" style="overflow-x: auto;">
					<table id="dashboardData" class="display" style="width: 100%">
						<thead>
							<tr>
								<th>Antrags-ID:</th>
								<th>Antragsnummer:</th>
								<th>zu erledigen bis:</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3" id="gesamtliste">
				<a class="button_special" id="liste_aeA" href="gesamtliste_AEA.php"><b>Liste aller ÄA</b></a>
			</div>
			<div class="col-md-3" id="neuanlage">
				<button class="button_special" id="newentry"><b>Neuer Eintrag</b></button>
			</div>
		</div>
		<div id="popover" class="popover">
			
		</div> 		
	    <div class="modal fade"  tabindex="-1" role="dialog"  aria-hidden="true" id="modal">	
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="title"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" id="modal_body">	
					
					</div>
					<div class="modal-footer justify-content-center" id="modal_footer">
		
					</div>
				</div>
			</div>
		</div>	
	</main>
	<footer class="footer mt-auto py-3">
		<div class="container text-center">
			<span class="text-container">SVLFG - Überwachung von Auslieferungsbeschreibungen</span>
		</div>
	</footer>

	<script src="<?php //echo $s_pathPre; ?>library/jquery/jquery-3.5.1.min.js"></script>
	<script src="<?php //echo $s_pathPre; ?>library/bootstrap-4.5.2-dist/js/bootstrap.bundle.min.js"></script>
	<script src="<?php //echo $s_pathPre; ?>library/dataTables/js/jquery.dataTables.min.js"></script>
	<script src="<?php //echo $s_pathPre; ?>library/dataTables/js/dataTables.bootstrap4.js"></script>
	<script src="<?php //echo $s_pathPre; ?>library/datepicker/bootstrap-datepicker.js"></script>
	<script src="<?php //echo $s_pathPre; ?>library/datepicker/bootstrap-datepicker.de.min.js"></script>
    
	<script src="js/ajax/ajax_select/ajaxCall_dashboard.js"></script>  
    <script src="js/ajax/ajax_insert/ajax_insert_start.js"></script> 
	
   	<script src="js/plausibility.js"></script>  
	<script src="js/popover_close.js"></script>		
   	<script src="js/toggle_class_selected.js"></script>  

	<script src="js/modal/index/datepicker_index.js"></script>
	<script src="js/modal/index/past_options_team.js"></script>   
   	<script src="js/modal/index/modal_newentry_start.js"></script>
	</body>

</html>
