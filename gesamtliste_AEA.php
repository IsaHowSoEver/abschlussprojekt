<?php
use \php_classes\cls_infoServer;


// Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	//	$path = str_replace ( "err/", "", $path );
	
	if (file_exists ( $path )) {
		require $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );

$o_server = new cls_infoServer();
$s_pathPre = $o_server->getS_path();
?>
<!doctype html>
<html lang="de" class="h-100">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<title>Gesamtliste AÄ</title>
<meta http-equiv="X-UA-Compatible" content="IE=10" />

<link href="<?php // echo $s_pathPre; ?>library/bootstrap-4.5.2-dist/css/bootstrap.css" rel="stylesheet">
<link href="<?php // echo $s_pathPre; ?>library/bootstrap-4.5.2-dist/css/sticky-footer-navbar.css" rel="stylesheet">
<link href="<?php  //echo $s_pathPre; ?>css/own_Bootstrap.css" rel="stylesheet">

<link rel="shortcut icon" type="image/x-icon" href="<?php //echo $s_pathPre; ?>img/favicon-32x32-keiml.png">
<link href="<?php // echo $s_pathPre; ?>css/site.css" rel="stylesheet">
<link href="<?php // echo $s_pathPre; ?>css/own_design.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="<?php //echo $s_pathPre; ?>library/dataTables/css/jquery.dataTables.css" />
<link rel="stylesheet" type="text/css" href="<?php //echo $s_pathPre; ?>library/dataTables/css/dataTables.bootstrap4.css" />

<link href="<?php // echo $s_pathPre; ?>css/own_DataTables.css" rel="stylesheet">

</head>
<body class="d-flex flex-column h-100">
	<header>
		<!-- Fixed navbar -->
		<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
			<a class="navbar-brand" id="headline" href="#">SVLFG - Überwachung von Auslieferungsbeschreibungen</a>  
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="navbar-brand" href="index.php">Zurück zur Startseite</a>
				</li>
			</ul>
		</nav>
	</header>
	<main role="main" class="flex-shrink-0">
		<div class="body_container">
		<p id="fehler"></p>
			<h1 class="mt-5 text-center">Gesamtliste offener Änderunganträge</h1>
		</div>
		<div class="table_container" style="overflow-x: auto;">
			<table id="gesamtAEA_data" class="display" style="width: 100%">
				<thead>
					<tr>
						<th>ID</th>
						<th>Antragsnummer</th>
						<th>zu erledigen bis</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<div class="to_dashboard">
			<a class="button_special" id="back_dashboard" href="index.php"><b>Zurück zur Startseite</b></a>
		</div>
	</main>
	<footer class="footer mt-auto py-3">
		<div class="container text-center">
			<span class="text-container">SVLFG - Überwachung von Auslieferungsbeschreibungen</span>
		</div>
	</footer>

	<script src="<?php //echo $s_pathPre; ?>library/jquery/jquery-3.5.1.min.js"></script>
	<script src="<?php //echo $s_pathPre; ?>library/bootstrap-4.5.2-dist/js/bootstrap.bundle.min.js"></script>
	<script src="<?php //echo $s_pathPre; ?>library/dataTables/js/jquery.dataTables.min.js"></script>
	<script src="<?php //echo $s_pathPre; ?>library/dataTables/js/dataTables.bootstrap4.js"></script>

	<script src="js/ajax/ajax_select/ajaxCall_gesamtliste.js"></script>

</body>
</html>

