<?php

namespace err\php_classes;

class cls_err {
	private $s_errNumber = "";
	private $s_errMessage = "";
	private $s_errDescription = "";

	public function __construct($s_errValue, $s_iniPath) {

		$this->getErrMessage ( $s_errValue, $s_iniPath );
	}

	private function getErrMessage($s_errValue, $s_iniPath) {

		$a_ini = $this->readIni ( $s_iniPath );
		$this->s_errNumber = $a_ini [0] [0];
		$this->s_errMessage = $a_ini [0] [1];
		$this->s_errDescription = $a_ini [0] [2];

		$i_key = array_search ( $s_errValue, array_column ( $a_ini, 0 ) );

		if ($i_key) {
			$this->s_errNumber = $a_ini [$i_key] [0];
			$this->s_errMessage = $a_ini [$i_key] [1];
			$this->s_errDescription = $a_ini [$i_key] [2];
		}
	}

	private function readIni($s_iniPath) {

		$i_row = 0;
		$a_ini = array ();
		if (($o_handle = fopen ( $s_iniPath, "r" )) !== FALSE) {
			while ( ($a_data = fgetcsv ( $o_handle, 1000, ";" )) !== FALSE ) {
				$a_ini [$i_row] = $a_data;
				$i_row ++;
			}
			fclose ( $o_handle );
		}
		return $a_ini;
	}

	public function getS_errNumber() {

		return $this->s_errNumber;
	}

	public function getS_errMessage() {

		return $this->s_errMessage;
	}

	public function getS_errDescription() {

		return $this->s_errDescription;
	}
}