<?php

namespace php_classes;

class cls_infoServer {
	
	private $s_uriPre;
	private $s_path = "/ueb-aea/";
	

	public function __construct() {
		
		$this->buildUriPre();
	}
	
	private function buildUriPre(){
		$s_port = ":".$this->getPort();
		$s_name = $this->getServer();
		
		if ($s_port == 80){$s_port ="";}
		
		$this->s_uriPre = "http://".$s_name.$s_port;
		
	}
	
	private function getServer(){
		return $_SERVER["SERVER_NAME"];
	}
	
	private function getPort(){
		return $_SERVER["SERVER_PORT"];
	}
	

	/**
	 * @return string
	 */
	public function getS_uriPre() {
		return $this->s_uriPre;
	}
	/**
	 * @return mixed
	 */
	public function getS_path() {
		return $this->s_path;
	}


}

 ?>
 					