<?php
// CSV-Datei in eine DB-Tabelle einlesen �ber PDO

// CSV-Datei einlesen
$csvContent = file("csv_Files/tbl_aea.csv", FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);

$a_data = [];
$s_delimiter = ";";

foreach ($csvContent as $input) {
    
    
    // Die Anzahl der Spalten anpassen
    list($s_column1, $s_column2, $s_column3, $s_column4) = explode($s_delimiter, $input);
    
    // Daten hinzuf�gen (Anzahl der Spalten anpassen) chr(13) steht für den Char für Carriage Return in der ASCII - Tabelle
    $a_data[] = "('" . $s_column1 . "','" . $s_column2 . "','" . $s_column3 . "','" . $s_column4 . "')" . chr(13);
}

// Verbindung zur Datenbank aufbauen
$db = new PDO('mysql:host=localhost;dbname=db_uebaea;charset=utf8', 'root', '');

// SQL-String zusammensetzen
// Die Anzahl der Spalten und Namen anpassen
$s_sql_query = "INSERT INTO `tbl_aea` (`ae_id`, `ae_nr`, `ae_link`, `ae_finish`) VALUES" . implode(",", $a_data) . ";";

// In die DB-Tabelle eintragen
if ($db->exec($s_sql_query)) {
    echo '<p>Die Daten wurden eingetragen.</p>';
}
else {
    print_r($db->errorInfo());
}




echo '<pre>' . $s_sql_query . '</pre>';
?>
