<?php
use model_db\db_query\cls_dbquery_detailansicht;

//Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path_substr = substr($path, 0, 9);
	
	if (file_exists ( $path )) {
		require_once $path;
	}else if($path_substr == "php_class"){
		$path = "../../".$path;
		require_once $path;	}else if($path_substr == "model_db/"){
			$path = "../../".$path;
			require_once $path;
		}
}
spl_autoload_register ( 'clsAutoloader' );
//GET Request bezieht sich auf den Ajax Call
$o_Result = new cls_dbquery_detailansicht($_GET["AEA-NR"], $_GET["TEAM-NAME"]);

$o_Result->getA_detail_data();

$s_result = json_encode($o_Result->getA_detail_data());
echo $s_result;


?>