<?php

use model_db\db_query\query_select_general_o_w;

//Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path_substr = substr($path, 0, 9);
	
	if (file_exists ( $path )) {
		require_once $path;
	}else if($path_substr == "php_class"){
		$path = "../../".$path;
		require_once $path;
	}else if($path_substr == "model_db/"){
		$path = "../../".$path;
		require_once $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );

//Deklaration der Abfrage, ob der ÄA vollständig ist
$s_select_team = "SELECT te_team FROM tbl_team";

//Durchführung der Abfrage
$o_query_team =  new query_select_general_o_w($s_select_team);

$a_teams = json_encode($o_query_team->getInhalte());

echo $a_teams;

?>