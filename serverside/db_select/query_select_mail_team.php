<?php

use model_db\db_query\query_select_general;

//Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path_substr = substr($path, 0, 9);
	
	if (file_exists ( $path )) {
		require_once $path;
	}else if($path_substr == "php_class"){
		$path = "../../".$path;
		require_once $path;
	}else if($path_substr == "model_db/"){
		$path = "../../".$path;
		require_once $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );

$s_team = $_POST["s_var_team"];

//Deklaration der Abfrage, ob der ÄA vollständig ist
$s_select_mail = "SELECT te_mail FROM tbl_team WHERE te_team =?";

$s_values_mail = [$s_team];
//Durchführung der Abfrage
$o_query_aea_anzahl =  new query_select_general($s_select_mail, $s_values_mail);

$a_teams = json_encode($o_query_aea_anzahl->getInhalte());

echo $a_teams;

?>