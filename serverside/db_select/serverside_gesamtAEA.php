<?php

use model_db\db_query\cls_dbquery_gesamtansicht;

//Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path_substr = substr($path, 0, 9);
	
	if (file_exists ( $path )) {
		require_once $path;
	}else if($path_substr == "php_class"){
		$path = "../../".$path;
		require_once $path;	}
		else if($path_substr == "model_db/"){
			$path = "../../".$path;
			require_once $path;
		}
}
spl_autoload_register ( 'clsAutoloader' );

$o_Result = new cls_dbquery_gesamtansicht();
$o_Result->getA_gesamtansicht_data();

$s_result = json_encode($o_Result->getA_gesamtansicht_data());
echo $s_result;

?>