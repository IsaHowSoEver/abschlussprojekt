<?php

use model_db\db_query\query_select_general;
use model_db\db_query\query_select_general_o_w;

//Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path_substr = substr($path, 0, 9);
	
	if (file_exists ( $path )) {
		require_once $path;
	}else if($path_substr == "php_class"){
		$path = "../../".$path;
		require_once $path;
	}else if($path_substr == "model_db/"){
		$path = "../../".$path;
		require_once $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );

$s_aea_nr = $_POST["s_var_aea_nr"];
$s_team = $_POST["s_var_team"];

//Deklaration der Abfrage(IDs)
$s_select_ids = "SELECT MAX(aea_id), MAX(te_id), MAX(at_id), MAX(co_id), MAX(da_id) FROM tbl_aea, tbl_team, tbl_aea_team, tbl_comment, tbl_dates";

//Deklaration der Abfrage(Einträge)
$s_select_aea = "SELECT aea_nr FROM tbl_aea WHERE aea_nr = ?";
$s_select_team = "SELECT te_team FROM tbl_team WHERE te_team = ?";
$s_values_aea = [$s_aea_nr];
$s_values_team = [$s_team];

//Deklaration der Abfrage(IDs)
$s_select_id_aea = "SELECT aea_id FROM tbl_aea WHERE aea_nr = ?";
$s_select_id_team = "SELECT te_id FROM tbl_team WHERE te_team = ?";
$s_values_id_aea = [$s_aea_nr];
$s_values_id_team = [$s_team];

//Durchführung der Abfragen
$o_query_id_aea_team =  new query_select_general_o_w($s_select_ids); 
$o_query_aea =  new query_select_general($s_select_aea, $s_values_aea); 
$o_query_team =  new query_select_general($s_select_team, $s_values_team); 

$o_query_id_aea =  new query_select_general($s_select_id_aea, $s_values_id_aea); 
$o_query_id_team =  new query_select_general($s_select_id_team,$s_values_id_team); 

$Daten_id_aea_team = json_encode($o_query_id_aea_team->getInhalte());
$Daten_aea = json_encode($o_query_aea->getInhalte());
$Daten_team = json_encode($o_query_team->getInhalte());

$Daten_id_aea = json_encode($o_query_id_aea->getInhalte());
$Daten_id_team = json_encode($o_query_id_team->getInhalte());
$length_id_aea = sizeof(json_decode($Daten_id_aea));
$length_id_team = sizeof(json_decode($Daten_id_team));

$DatenGesamt[] = json_decode($Daten_aea);
$DatenGesamt[] = json_decode($Daten_team);
$DatenGesamt[] = json_decode($Daten_id_aea_team);

//Prüfung ob Einträge schon vorhanden sind(>0=ja 0=nein) und Ergebnisse in einem Array speichern
if($length_id_aea > 0 ){
	$DatenGesamt[] = json_decode($Daten_id_aea);
}else{
	$DatenGesamt[] = json_decode('["nein"]');
}
if($length_id_team > 0 ){
	$DatenGesamt[] = json_decode($Daten_id_team);
}else{
	$DatenGesamt[] = json_decode('["nein"]');
}

$Daten_merge = json_encode($DatenGesamt);

echo $Daten_merge;

?>