<?php

use model_db\db_query\query_select_general;
use model_db\db_query\query_select_general_o_w;

//Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path_substr = substr($path, 0, 9);
	
	if (file_exists ( $path )) {
		require_once $path;
	}else if($path_substr == "php_class"){
		$path = "../../".$path;
		require_once $path;
	}else if($path_substr == "model_db/"){
		$path = "../../".$path;
		require_once $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );

$i_aea_id = $_POST["i_var_aea_id"];
$s_team = $_POST["s_var_team"];

//Deklaration der Abfragen
$s_select_id_date = "SELECT MAX(da_id) FROM tbl_dates";

$s_select_id_aea_team = "SELECT at_id FROM tbl_aea_team".
						" INNER JOIN tbl_aea ON tbl_aea.aea_id = tbl_aea_team.at_id_aea".
						" INNER JOIN tbl_team ON tbl_team.te_id = tbl_aea_team.at_id_team".
						" WHERE tbl_team.te_team = ?".
						" AND tbl_aea.aea_id = ?";
$s_values = [$s_team, $i_aea_id];

//Durchführung der Abfragen
$o_query_id_date =  new query_select_general_o_w($s_select_id_date);
$o_query_id_aea_team =  new query_select_general($s_select_id_aea_team, $s_values);

$Daten_id_date = json_encode($o_query_id_date->getInhalte());
$Daten_id_aea_team = json_encode($o_query_id_aea_team->getInhalte());

//Ergebnisse in einem Array speichern
$DatenGesamt[] = json_decode($Daten_id_date);
$DatenGesamt[] = json_decode($Daten_id_aea_team);

$Daten_merge = json_encode($DatenGesamt);

echo $Daten_merge;

?>