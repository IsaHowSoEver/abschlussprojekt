<?php

use model_db\db_query\query_insert_general;

//Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path_substr = substr($path, 0, 9);
	
	if (file_exists ( $path )) {
		require_once $path;
	}else if($path_substr == "php_class"){
		$path = "../../".$path;
		require_once $path;
	}else if($path_substr == "model_db/"){
		$path = "../../".$path;
		require_once $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );

$comment = $_POST["s_var_comment"];
$todo = $_POST["s_var_todo"];
$i_s_id_max_comment = $_POST["i_s_var_id_max_comment"];
$i_s_id_aea_team = $_POST["i_s_var_id_aea_team"];


//Prüfen welche Kommentare gefüllt sind & Statement deklarieren
if($comment == ""){
	$s_insert_comment = "INSERT INTO tbl_comment (co_id, co_comment, co_todo, co_id_aea_team) VALUES(?, ?, ?, ?)";
	$s_values = [$i_s_id_max_comment, '', $todo, $i_s_id_aea_team];
	
}else if($todo == ""){
	$s_insert_comment = "INSERT INTO tbl_comment (co_id, co_comment, co_todo, co_id_aea_team) VALUES(?, ?, ?, ?)";
	$s_values= [$i_s_id_max_comment, $comment, '', $i_s_id_aea_team];
	
}else{
	$s_insert_comment = "INSERT INTO tbl_comment (co_id, co_comment, co_todo, co_id_aea_team) VALUES(?, ?, ?, ?)";
	$s_values = [$i_s_id_max_comment, $comment, $todo, $i_s_id_aea_team];
	
}

//Ausführung des Statements
$o_query_comment =  new query_insert_general($s_insert_comment, $s_values);

?>