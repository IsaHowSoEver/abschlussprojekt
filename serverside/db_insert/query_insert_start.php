<?php

use model_db\db_query\query_insert_general;

//Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path_substr = substr($path, 0, 9);
	
	if (file_exists ( $path )) {
		require_once $path;
	}else if($path_substr == "php_class"){
		$path = "../../".$path;
		require_once $path;
	}else if($path_substr == "model_db/"){
		$path = "../../".$path;
		require_once $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );

$s_aea_nr = $_POST["s_var_aea_nr"];
$s_team = $_POST["s_var_team"];
$s_mail = $_POST["s_var_mail"];
$s_comment = $_POST["s_var_comment"];
$s_todo = $_POST["s_var_todo"];
$d_maildate = $_POST["s_var_maildate"];
$d_deadline = $_POST["s_var_deadline"];
$d_finished = $_POST["s_var_finished"];
$i_aea = $_POST["i_var_aea"];
$i_team = $_POST["i_var_team"];
$i_s_id_aea = $_POST["i_s_var_id_aea"];
$i_s_id_team = $_POST["i_s_var_id_team"];
$i_s_id_max_aea = $_POST["i_s_var_id_max_aea"];
$i_s_id_max_team = $_POST["i_s_var_id_max_team"];
$i_s_id_max_comment = $_POST["i_s_var_id_max_comment"];
$i_s_id_max_date = $_POST["i_s_var_id_max_date"];
$i_s_id_aea_team = $_POST["i_s_var_id_aea_team"];

//Prüfen, ob aea und team schon vorhanden sind und Deklaration der Statements (!=0: vorhanden =0:nicht vorhanden)
if($i_aea == 0 && $i_team != 0){
	$s_insert_aea =  "INSERT INTO tbl_aea (aea_id, aea_nr, aea_link, aea_finish) VALUES(?, ?, ?, ?)";
	$s_insert_aea_team = "INSERT INTO tbl_aea_team (at_id, at_id_aea, at_id_team) VALUES(?, ?, ?)";
	$s_insert_team = "1";
	
	$s_values_aea = [$i_s_id_max_aea, $s_aea_nr, 'Link', '0'];
	$s_values_aea_team = [$i_s_id_aea_team, $i_s_id_max_aea, $i_s_id_team];
	
}else if($i_aea != 0 && $i_team == 0){
	$s_insert_aea = "1";
	$s_insert_aea_team = "INSERT INTO tbl_aea_team (at_id, at_id_aea, at_id_team) VALUES(?, ?, ?)";
	$s_insert_team = "INSERT INTO tbl_team (te_id, te_team, te_mail) VALUES(?, ?, ?)";
	
	$s_values_aea_team = [$i_s_id_aea_team, $i_s_id_aea, $i_s_id_max_team];
	$s_values_team = [$i_s_id_max_team, $s_team, $s_mail];
	
}else if($i_aea == 0 &&  $i_team == 0){
	$s_insert_aea =  "INSERT INTO tbl_aea (aea_id, aea_nr, aea_link, aea_finish) VALUES(?, ?, ?, ?)";
	$s_insert_aea_team = "INSERT INTO tbl_aea_team (at_id, at_id_aea, at_id_team) VALUES(?, ?, ?)";
	$s_insert_team = "INSERT INTO tbl_team (te_id, te_team, te_mail) VALUES(?, ?, ?)";
	
	$s_values_aea = [$i_s_id_max_aea, $s_aea_nr, 'Link', '0'];
	$s_values_aea_team = [$i_s_id_aea_team, $i_s_id_max_aea, $i_s_id_max_team];
	$s_values_team = [$i_s_id_max_team, $s_team, $s_mail];
	
}else if($i_aea != 0 && $i_team != 0){
	$s_insert_aea = "1";
	$s_insert_aea_team = "INSERT INTO tbl_aea_team (at_id, at_id_aea, at_id_team) VALUES(?, ?, ?)";
	$s_insert_team = "1";
	
	$s_values_aea_team = [$i_s_id_aea_team, $i_s_id_aea, $i_s_id_team];
}

//Prüfen ob die Kommentare gefüllt sind und Deklaration des Statements
if($s_comment == "" && $s_todo == ""){
	$s_insert_comment = "0";
}else if($s_comment == ""){
	$s_insert_comment = "INSERT INTO tbl_comment (co_id, co_comment, co_todo, co_id_aea_team) VALUES(?, ?, ?, ?)";
	$s_values_comment = [$i_s_id_max_comment, '', $s_todo, $i_s_id_aea_team];
	
}else if($s_todo == ""){
	$s_insert_comment = "INSERT INTO tbl_comment (co_id, co_comment, co_todo, co_id_aea_team) VALUES(?, ?, ?, ?)";	
	$s_values_comment = [$i_s_id_max_comment, $s_comment, '', $i_s_id_aea_team];
	
}else{
	$s_insert_comment = "INSERT INTO tbl_comment (co_id, co_comment, co_todo, co_id_aea_team) VALUES(?, ?, ?, ?)";
	$s_values_comment = [$i_s_id_max_comment, $s_comment, $s_todo, $i_s_id_aea_team];
}

//Prüfen ob das Datum "Erledigt" und "Mail gesendet" gefüllt sind  und Deklaration des Statements
if($d_finished == "0000-00-00" && $d_maildate != "0000-00-00"){
	$s_insert_date = "INSERT INTO tbl_dates (da_id, da_dateMail, da_deadline, da_id_aea_team, da_finished) VALUES(?, ?, ?, ?, ?)";	
	$s_values_date = [$i_s_id_max_date, $d_maildate, $d_deadline, $i_s_id_aea_team, ''];
	
}else if($d_finished != "0000-00-00" && $d_maildate == "0000-00-00"){
	$s_insert_date = "INSERT INTO tbl_dates (da_id, da_dateMail, da_deadline, da_id_aea_team, da_finished) VALUES(?, ?, ?, ?, ?)";	
	$s_values_date = [$i_s_id_max_date, '', $d_deadline, $i_s_id_aea_team, $d_finished];
	
}else if($d_finished == "0000-00-00" && $d_maildate == "0000-00-00"){
	$s_insert_date = "INSERT INTO tbl_dates (da_id, da_dateMail, da_deadline, da_id_aea_team, da_finished) VALUES(?, ?, ?, ?, ?)";	
	$s_values_date = [$i_s_id_max_date, '', $d_deadline, $i_s_id_aea_team, ''];
	
}else{
	$s_insert_date = "INSERT INTO tbl_dates (da_id, da_dateMail, da_deadline, da_id_aea_team, da_finished) VALUES(?, ?, ?, ?, ?)";	
	$s_values_date = [$i_s_id_max_date, $d_maildate, $d_deadline, $i_s_id_aea_team, $d_finished];
}

//Ausführung der Statements
if($s_insert_aea != "1"){		
	$o_query_aea =  new query_insert_general($s_insert_aea, $s_values_aea);
}
if($s_insert_team != "1"){
	$o_query_team =  new query_insert_general($s_insert_team, $s_values_team); 
}
$o_query_aea_team =  new query_insert_general($s_insert_aea_team, $s_values_aea_team); 
if($s_insert_comment != "0"){
	$o_query_comment =  new query_insert_general($s_insert_comment, $s_values_comment);
}
$o_query_date =  new query_insert_general($s_insert_date, $s_values_date); 
