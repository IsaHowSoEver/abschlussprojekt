<?php

use model_db\db_query\query_insert_general;

//Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path_substr = substr($path, 0, 9);
	
	if (file_exists ( $path )) {
		require_once $path;
	}else if($path_substr == "php_class"){
		$path = "../../".$path;
		require_once $path;
	}else if($path_substr == "model_db/"){  
		$path = "../../".$path;
		require_once $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );

$d_maildate = $_POST["s_var_maildate"];
$d_date_deadline = $_POST["s_var_date_deadline"];
$id_max_date = $_POST["i_s_var_id_max_date"];
$i_s_id_aea_team = $_POST["i_s_var_id_aea_team"];

//Deklaration des Statements
$s_insert_date = "INSERT INTO tbl_dates (da_id, da_dateMail, da_deadline, da_id_aea_team, da_finished) VALUES(?, ?, ?, ?, ?)";
$s_values = [$id_max_date, $d_maildate, $d_date_deadline, $i_s_id_aea_team, ""];

//Ausführung des Statements
$o_query_date =  new query_insert_general($s_insert_date, $s_values);

?>