<?php

use model_db\db_query\query_insert_general;

//Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path_substr = substr($path, 0, 9);
	
	if (file_exists ( $path )) {
		require_once $path;
	}else if($path_substr == "php_class"){
		$path = "../../".$path;
		require_once $path;
	}else if($path_substr == "model_db/"){
		$path = "../../".$path;
		require_once $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );

$d_deadline = $_POST["s_var_deadline"];
$d_finished = $_POST["s_var_finished"];

$i_aea_id = $_POST["i_var_aea_id"];
$s_team = $_POST["s_var_team"];

//Prüfung ob Erledigt gefüllt ist, Deklaration der Statements und Ausführung des Statements
if($d_finished == "0000-00-00"){
	$updatedeadline = "UPDATE tbl_dates".
					  " INNER JOIN tbl_aea_team ON tbl_dates.da_id_aea_team = tbl_aea_team.at_id".
					  " INNER JOIN tbl_aea ON tbl_aea.aea_id = tbl_aea_team.at_id_aea".
					  " SET tbl_dates.da_deadline = ?".
					  " WHERE  tbl_aea.aea_id = ?";
	
	$s_values_deadline = [$d_deadline, $i_aea_id];
	
	$o_query_deadline =  new query_insert_general($updatedeadline, $s_values_deadline);
}else{
	$updatedeadline = "UPDATE tbl_dates".
					  " INNER JOIN tbl_aea_team ON tbl_dates.da_id_aea_team = tbl_aea_team.at_id".
					  " INNER JOIN tbl_aea ON tbl_aea.aea_id = tbl_aea_team.at_id_aea".
					  " SET tbl_dates.da_deadline = ?".
					  " WHERE  tbl_aea.aea_id = ?";
			
	$s_values_deadline = [$d_deadline, $i_aea_id];
	
	$updatefinished = "UPDATE tbl_dates".
					  " INNER JOIN tbl_aea_team ON tbl_dates.da_id_aea_team = tbl_aea_team.at_id".
					  " INNER JOIN tbl_team ON tbl_team.te_id = tbl_aea_team.at_id_team".
					  " INNER JOIN tbl_aea ON tbl_aea.aea_id = tbl_aea_team.at_id_aea".
					  " SET tbl_dates.da_finished = ?".
					  " WHERE  tbl_aea.aea_id = ?".
					  " AND tbl_team.te_team = ?";
	
	$s_values_finished = [$d_finished, $i_aea_id, $s_team];
	
	$o_query_deadline =  new query_insert_general($updatedeadline, $s_values_deadline); 
	$o_query_finished =  new query_insert_general($updatefinished, $s_values_finished); 
}

?>


