<?php

use model_db\db_query\query_insert_general;

//Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path_substr = substr($path, 0, 9);
	
	if (file_exists ( $path )) {
		require_once $path;
	}else if($path_substr == "php_class"){
		$path = "../../".$path;
		require_once $path;
	}else if($path_substr == "model_db/"){
		$path = "../../".$path;
		require_once $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );

$s_team = $_POST["s_var_team"];
$s_mail = $_POST["s_var_mail"];

$s_team_edit = $_POST["s_var_team_edit"];
$s_mail_edit = $_POST["s_var_mail_edit"];

//Deklaration des Statements
$s_update_team = "UPDATE tbl_team SET te_team = ?, te_mail = ? WHERE te_team = ? AND te_mail = ?";
$s_values = [$s_team_edit, $s_mail_edit, $s_team, $s_mail];

//Ausführung des Statements
$o_query_date =  new query_insert_general($s_update_team, $s_values);

?>

