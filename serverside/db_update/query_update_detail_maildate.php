<?php

use model_db\db_query\query_insert_general;

//Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path_substr = substr($path, 0, 9);
	
	if (file_exists ( $path )) {
		require_once $path;
	}else if($path_substr == "php_class"){
		$path = "../../".$path;
		require_once $path;
	}else if($path_substr == "model_db/"){
		$path = "../../".$path;
		require_once $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );

$d_maildate = $_POST["s_var_maildate"];
$d_maildate_old = $_POST["s_var_maildate_old"];

$i_aea_id = $_POST["i_var_aea_id"];
$s_team = $_POST["s_var_team"];

//Deklaration des Statements
$update = "UPDATE tbl_dates".
		  " INNER JOIN tbl_aea_team ON tbl_aea_team.at_id = tbl_dates.da_id_aea_team".
		  " INNER JOIN tbl_aea ON tbl_aea.aea_id = tbl_aea_team.at_id_aea".
		  " INNER JOIN tbl_team ON tbl_team.te_id = tbl_aea_team.at_id_team".
		  " SET tbl_dates.da_datemail = ?".
		  " WHERE tbl_aea.aea_id = ?".
		  " AND tbl_team.te_team = ?".
		  " AND tbl_dates.da_dateMail = ?";

$s_values = [$d_maildate, $i_aea_id, $s_team, $d_maildate_old];

//Ausführung des Statements
$o_query_maildate =  new query_insert_general($update, $s_values); 

?>
