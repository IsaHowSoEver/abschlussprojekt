<?php

use model_db\db_query\query_insert_general;

//Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path_substr = substr($path, 0, 9);
	
	if (file_exists ( $path )) {
		require_once $path;
	}else if($path_substr == "php_class"){
		$path = "../../".$path;
		require_once $path;
	}else if($path_substr == "model_db/"){
		$path = "../../".$path;
		require_once $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );

$i_aea_id = $_POST["i_var_aea_id"];

//Alle Anträge sind erledigt. aea_finished wird auf 1 gesetzt
$s_update_finish = "Update tbl_aea SET aea_finish = ? WHERE aea_id = ?";

$s_values = ["1", $i_aea_id];

$o_query_update_finish =  new query_insert_general($s_update_finish, $s_values);
	
?>