<?php

use model_db\db_query\query_insert_general;

//Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path_substr = substr($path, 0, 9);
	
	if (file_exists ( $path )) {
		require_once $path;
	}else if($path_substr == "php_class"){
		$path = "../../".$path;
		require_once $path;
	}else if($path_substr == "model_db/"){
		$path = "../../".$path;
		require_once $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );

$s_comment = $_POST["s_var_comment"];
$s_todo = $_POST["s_var_todo"];
$s_comment_old = $_POST["s_var_comment_old"];
$s_todo_old = $_POST["s_var_todo_old"];

$i_aea_id = $_POST["i_var_aea_id"];
$s_team = $_POST["s_var_team"];

//Deklaration des Statements
$update = "UPDATE tbl_comment".
		  " INNER JOIN tbl_aea_team ON tbl_aea_team.at_id = tbl_comment.co_id_aea_team".
		  " INNER JOIN tbl_aea ON tbl_aea.aea_id = tbl_aea_team.at_id_aea".
		  " INNER JOIN tbl_team ON tbl_team.te_id = tbl_aea_team.at_id_team".
		  " SET tbl_comment.co_comment = ?, tbl_comment.co_todo = ?".
		  " WHERE tbl_aea.aea_id = ?".
		  " AND tbl_team.te_team = ?".
		  " AND tbl_comment.co_comment = ?".
		  " AND tbl_comment.co_todo = ?";

$s_values = [$s_comment, $s_todo, $i_aea_id, $s_team, $s_comment_old, $s_todo_old];

//Ausführung des Statements
$o_query_comment =  new query_insert_general($update, $s_values); 
?>

