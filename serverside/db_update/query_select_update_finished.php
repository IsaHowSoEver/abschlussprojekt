<?php

use model_db\db_query\query_select_general;

//Autoload Klassen
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path_substr = substr($path, 0, 9);
	
	if (file_exists ( $path )) {
		require_once $path;
	}else if($path_substr == "php_class"){
		$path = "../../".$path;
		require_once $path;
	}else if($path_substr == "model_db/"){
		$path = "../../".$path;
		require_once $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );


$i_aea_id = $_POST["i_var_aea_id"];

//Deklaration der Abfrage, ob der ÄA vollständig ist
$s_select_aea_anzahl = "SELECT COUNT(*) FROM tbl_dates".
					   " INNER JOIN tbl_aea_team ON tbl_dates.da_id_aea_team = tbl_aea_team.at_id".
					   " INNER JOIN tbl_aea ON tbl_aea.aea_id = tbl_aea_team.at_id_aea".
					   " WHERE tbl_aea.aea_id = ?".
					   " AND tbl_dates.da_finished = ?";

$s_values = [$i_aea_id, "0000-00-00"];

//Durchführung der Abfrage
$o_query_aea_anzahl =  new query_select_general($s_select_aea_anzahl, $s_values);

$aea_anzahl = json_encode($o_query_aea_anzahl->getInhalte());

echo $aea_anzahl;

?>