$("#modal").on('shown.bs.modal', function () {	
	$('#btn_update').on('click', function () {
	
//Abruf der Daten vom Modal
		var s_team_edit = document.getElementById("0").value;
		var s_mail_edit = document.getElementById("1").value;
						
//Prüfung ob die Felder gefüllt sind!!
		if(s_team_edit == "" || s_mail_edit == ""){
				$('#popover').popover({
					title: "Fehler",
					placement: "auto",
					content: "Bitte füllen sie alle Pflichtfelder aus!",
					html:true
				});
				$('#popover').popover('show');
		}else{
//Plausiprüfung laden
			$.getScript('js/plausibility.js', function(){});
			var b_xss_update_team = xss_script_update(s_team_edit);			
			var b_xss_update_mail = xss_script_update(s_mail_edit);
			var b_mail_plausi = mail_plausi(s_mail_edit);
			
			var b_team_length = plausi_length_team_edit_entry(s_team_edit, s_mail_edit);
			
			if(b_xss_update_team == true && b_mail_plausi == true && b_xss_update_mail == true && b_team_length == true){
				update_team(s_team_edit, s_mail_edit);
			}else{
				$('#popover').popover({
					title: "Fehler",
					placement: "auto",
					content: "Bitte überprüfen sie ihre Eingaben!",
					html:true
				});
				$('#popover').popover('show');				
			}
		}	 
	});
});	

function update_team(s_team_edit, s_mail_edit){
	var s_team = window.sessionStorage.getItem("team");
	var s_mail = window.sessionStorage.getItem("mail");
	
	$.ajax({
		method: "POST",
  		url: "serverside/db_update/query_update_team.php",
		data:{s_var_team:s_team,
			  s_var_mail:s_mail,
			  s_var_team_edit:s_team_edit,
			  s_var_mail_edit:s_mail_edit},
		})
		.done(function(data) {
			var s_fehler = data.substring(6, 12);
			var s_url_fehler = data.substring(13);
			if(s_fehler != "Fehler"){
				$('#popover').popover({
					title: "Durchführung",
					placement: "auto",
					content: "Ihre Änderung wurde erfolgreich durchgeführt.",
					html:true
				});
				$('#popover').popover('show');
				
				$('#antragData').DataTable().destroy();		
				$.getScript('js/ajax/ajax_select/ajaxCall_antragsdaten.js', function(){});
				$('#modal').modal("hide");
			}else{
				location.href = s_url_fehler;
			}					
		})
		.fail(function(data){
			$.getScript('js/error_forwarding.js', function(){});
			error_forwarding(data);
	});		
}	


