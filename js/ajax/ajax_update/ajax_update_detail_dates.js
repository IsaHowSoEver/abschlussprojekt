$("#modal").on('shown.bs.modal', function () {	
	$('#btn_update_dates').on('click', function () {
	
//Abruf der Daten vom Modal
		var d_deadline = document.getElementById("0").value;
		var d_finished = document.getElementById("1").value;
		
//Array für die URL Parameter deklarieren
		var a_value = new Array();	
//Team und ÄA abrufen aus der URL
		var s_url = String(window.location);
	    if(window.location.search != "") {
	//Parameter von der URL trennen
		var a_split = s_url.split("?");
			var a_parameter  = a_split[1].split("&");
			for(i = 0; i < a_parameter.length; i++) {
				a_value[i] = a_parameter[i].split("=");
			}
			var i_aea_id = a_value[0][1];
			var s_team = a_value[2][1];
	    }
						
//Prüfung ob das Pflicht-Feld gefüllt ist!!
		if(s_deadline == "0000-00-00"){
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Bitte füllen sie das Feld aus!",
				html:true
			});
			$('#popover').popover('show');
		}else{
//Plausiprüfung laden
			$.getScript('js/plausibility.js', function(){});	
			var s_finished = "Erledigt";
			var s_deadline = "Termin";
			if(d_finished == "0000-00-00"){		
				var b_deadline_plausi = plausi_date(d_deadline, s_deadline);
				var b_deadline = plausi_date_past(d_deadline, s_deadline)
				if(b_deadline_plausi == true && b_deadline == true){
					update_dates(d_deadline, d_finished, s_team, i_aea_id);
				}
			}else{		
				var b_deadline_format = plausi_date(d_deadline, s_deadline);
				var b_deadline_past = plausi_date_past(d_deadline, s_deadline)
				
				var b_finished_format = plausi_date(d_finished, s_finished);
				var b_finished_past = plausi_date_past(d_finished, s_finished);
				
				var b_finished_future = plausi_date_future(d_finished, s_finished);				
				
				
				if(b_deadline_format == true && b_deadline_past == true && b_finished_format == true && b_finished_past == true && b_finished_future == true){
					update_dates(d_deadline, d_finished, s_team, i_aea_id);
				}else{
					$('#popover').popover({
						title: "Fehler",
						placement: "auto",
						content: "Bitte überprüfen sie ihre Eingaben!",
						html:true
					});
					$('#popover').popover('show');
				}		
			}
		}	 
	});
});

function update_dates(d_deadline, d_finished, s_team, i_aea_id){
	
	$.ajax({
		method: "POST",
  		url: "serverside/db_update/query_update_detail_dates.php",
		data:{s_var_deadline:d_deadline,
			  s_var_finished:d_finished,
              i_var_aea_id:i_aea_id,
			  s_var_team:s_team},
		})
		.done(function(data){
			var s_fehler = data.substring(6, 12);
			var s_url_fehler = data.substring(13);
			if(s_fehler != "Fehler"){
				select_update_finished(i_aea_id);	
			}else{
				location.href = s_url_fehler;
			}				
		})
		.fail(function(data){
			$.getScript('js/ajax/ajax_error.js', function(){});
			error_forwarding(data);
	});		
}	

function select_update_finished(i_aea_id){
	
	$.ajax({
		method: "POST",
  		url: "serverside/db_update/query_select_update_finished.php",
		data:{i_var_aea_id:i_aea_id},
		})
		.done(function(data){
			var s_fehler = data.substring(6, 12);
			var s_url_fehler = data.substring(13);
			if(s_fehler != "Fehler"){			
				var a_inhalte = JSON.parse(data);	
				var i_finished = a_inhalte[0];
				
				if(i_finished == 0){
					update_finished(i_aea_id);
				}else{
					$('#popover').popover({
						title: "Durchführung",
						placement: "auto",
						content: "Ihre Änderung wurde erfolgreich durchgeführt.",
						html:true
					});
					$('#popover').popover('show');
						
					$('#detail_data').DataTable().destroy();
					$('#comment_data').DataTable().destroy();	
					$('#mail_data').DataTable().destroy();		
					$.getScript('js/ajax/ajax_select/ajaxCall_detailansicht.js', function(){});
					$('#modal').modal("hide");
				};
			}else{
				location.href = s_url_fehler;
			}	

		})
		.fail(function(data){
			$.getScript('js/error_forwarding.js', function(){});
			error_forwarding(data);
	});		
}
	
function update_finished(i_aea_id){
	$.ajax({
		method: "POST",
  		url: "serverside/db_update/query_update_finished.php",
		data:{i_var_aea_id:i_aea_id},
		})
		.done(function(data) {
			var s_fehler = data.substring(6, 12);
			var s_url_fehler = data.substring(13);
			if(s_fehler != "Fehler"){	
				$('#popover').popover({
					title: "Durchführung",
					placement: "auto",
					content: "Ihre Änderung wurde erfolgreich durchgeführt.",
					html:true
				});
				$('#popover').popover('show');		
				$('#detail_data').DataTable().destroy();
				$('#comment_data').DataTable().destroy();	
				$('#mail_data').DataTable().destroy();		
				$.getScript('js/ajax/ajax_select/ajaxCall_detailansicht.js', function(){});
			}else{
				location.href = s_url_fehler;
			}							
		})
		.fail(function(data){
			$.getScript('js/error_forwarding.js', function(){});
			error_forwarding(data);
	});
}

