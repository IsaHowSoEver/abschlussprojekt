$("#modal").on('shown.bs.modal', function () {	
	$('#btn_update_comment').on('click', function () {
	
//Abruf der Daten vom Modal
	var s_comment = document.getElementById("0").value;
	var s_todo = document.getElementById("1").value;
//	var comment_absatz = comment.replace("↵","<br>");

//Array für die URL Parameter deklarieren
	var a_value = new Array();	
//Team und ÄA abrufen aus der URL
	var s_url = String(window.location);
    if(window.location.search != "") {
//Parameter von der URL trennen
		var a_split = s_url.split("?");
		var a_parameter  = a_split[1].split("&");
		for(i = 0; i < a_parameter.length; i++) {
			a_value[i] = a_parameter[i].split("=");
		}
		var i_aea_id = a_value[0][1];
		var s_team = a_value[2][1];
    }
							
//Prüfung ob mindestens ein Feld gefüllt ist!!
	if(s_comment == "" && s_todo == ""){
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Mindestens ein Feld müssen sie ausfüllen!",
			html:true
		});
		$('#popover').popover('show');
	}else{	
//Plausiprüfung laden
		$.getScript('js/plausibility.js', function(){});
		var b_xss_update_comment= xss_script_update(s_comment);
		var b_xss_update_todo= xss_script_update(s_todo);
		
		var b_comment_length = plausi_length_comment(s_comment, s_todo);
		
		if(b_xss_update_comment == true && b_xss_update_todo == true && b_comment_length == true){
			update_comment(s_comment, s_todo, s_team, i_aea_id);
		}else{
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Bitte überprüfen sie ihre Eingaben!",
				html:true
			});
			$('#popover').popover('show');			
		}
	}		 
});
});	


function update_comment(s_comment, s_todo, s_team, i_aea_id){
	var s_comment_old = window.sessionStorage.getItem("comment");
	var s_todo_old = window.sessionStorage.getItem("todo");
	
	$.ajax({
		method: "POST",
  		url: "serverside/db_update/query_update_detail_comment.php",
		data:{s_var_comment_old:s_comment_old,
			  s_var_todo_old:s_todo_old,
			  s_var_comment:s_comment,
		      s_var_todo:s_todo,
              i_var_aea_id:i_aea_id,
			  s_var_team:s_team},
		})
		.done(function(data) {
			var s_fehler = data.substring(6, 12);
			var s_url_fehler = data.substring(13);
			if(s_fehler != "Fehler"){
				$('#popover').popover({
					title: "Durchführung",
					placement: "auto",
					content: "Ihre Änderung wurde erfolgreich durchgeführt.",
					html:true
				});
				$('#popover').popover('show');
									
				$('#detail_data').DataTable().destroy();
				$('#comment_data').DataTable().destroy();	
				$('#mail_data').DataTable().destroy();		
				$.getScript('js/ajax/ajax_select/ajaxCall_detailansicht.js', function(){});
				$('#modal').modal("hide");
			}else{
				location.href = s_url_fehler;
			}					
		})
		.fail(function(data){
			$.getScript('js/error_forwarding.js', function(){});
			error_forwarding(data);
	});		
}	


