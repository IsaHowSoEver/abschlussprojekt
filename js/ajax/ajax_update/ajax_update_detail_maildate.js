$("#modal").on('shown.bs.modal', function () {	
	$('#btn_update_maildate').on('click', function () {
	
//Abruf der Daten vom Modal vor dem Speichern
		var d_maildate = document.getElementById("0").value;
		
//Array für die URL Parameter deklarieren
		var a_value = new Array();	
//Team und ÄA abrufen aus der URL
		var s_url = String(window.location);
	    if(window.location.search != "") {
//Parameter von der URL trennen
			var a_split = s_url.split("?");
			var a_parameter  = a_split[1].split("&");
			for(i = 0; i < a_parameter.length; i++) {
				a_value[i] = a_parameter[i].split("=");
			}
			var i_aea_id = a_value[0][1];
			var s_team = a_value[2][1];
	    }
						
//Prüfung ob das Feld gefüllt ist!!
		if(d_maildate == "0000-00-00"){
			$('#popover').popover({
				title: "Durchführung",
				placement: "auto",
				content: "Bitte füllen sie das Feld aus!",
				html:true
			});
			$('#popover').popover('show');
		}else{
//Plausiprüfung laden
			$.getScript('js/plausibility.js', function(){});	
			var s_maildate = "Mail gesendet";
			var b_maildate_format = plausi_date(d_maildate, s_maildate);
			var b_maildate_past = plausi_date_past(d_maildate, s_maildate);
			var b_maildate_future = plausi_date_future(d_maildate, s_maildate);
			
			if(b_maildate_format == true && b_maildate_past == true && b_maildate_future == true){
				update_maildate(d_maildate, s_team, i_aea_id);
			}else{
				$('#popover').popover({
					title: "Durchführung",
					placement: "auto",
					content: "Bitte überprüfen sie ihre Eingaben!",
					html:true
				});
				$('#popover').popover('show');
			}
		}	 
	});
});	


function update_maildate(d_maildate, s_team, i_aea_id){
	var d_maildate_old = window.sessionStorage.getItem("maildate");
	
	$.ajax({
		method: "POST",
  		url: "serverside/db_update/query_update_detail_maildate.php",
		data:{s_var_maildate:d_maildate,
		      s_var_maildate_old:d_maildate_old,
              i_var_aea_id:i_aea_id,
			  s_var_team:s_team},
		})
		.done(function(data) {
			var s_fehler = data.substring(6, 12);
			var s_url_fehler = data.substring(13);
			if(s_fehler != "Fehler"){	
				$('#popover').popover({
					title: "Durchführung",
					placement: "auto",
					content: "Ihre Änderung wurde erfolgreich durchgeführt.",
					html:true
				});
				$('#popover').popover('show');	
							
				$('#detail_data').DataTable().destroy();
				$('#comment_data').DataTable().destroy();	
				$('#mail_data').DataTable().destroy();		
				$.getScript('js/ajax/ajax_select/ajaxCall_detailansicht.js', function(){});
				$('#modal').modal("hide");
			}else{
				location.href = s_url_fehler;
			}					
		})
		.fail(function(data){
			$.getScript('js/error_forwarding.js', function(){});
			error_forwarding(data);
	});		
}	


