$(document).ready(function() {
//ÄA ID aus der URL auslesen
	var s_url = String(window.location);
    if(window.location.search != "") {
//Parameter von der URL trennen
		var a_split = s_url.split("?");
		var a_parameter  = a_split[1].split("&");
		var a_value_id = a_parameter[0].split("=");
		var i_aea_id = a_value_id[1];
		
		var a_value_nr = a_parameter[1].split("=");
		var s_aea_nr = a_value_nr[1];
	
    }
	var s_aea_nr=$("#AEA_nr").text();
  $('#antragData').DataTable( {
	    "responsive": true,
        "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]],
        language: {
	         url: "js/json/de.json"
        },
        "ajax": {
            "url": "serverside/db_select/serverside_AEAdata.php",
        	"data": {
	  				"AEA-ID": i_aea_id
   			 },
        		"dataSrc": ""
				//Fehlerabfangen
			},
            //hier werden die Spaltenüberschriften erstellt           
            "columns": [ 
	        	{"data": "te_team"},
                {"data": "te_mail"}
             ],
			"columnDefs": [
           	{
				"targets": [ 0 ],
             	"visible": true,
             	"searchable": true},
            {
                "targets": [ 1 ],
                "visible": true,
                "searchable": true
            }
			]
	});
						
   // Seite verlinken
	$('#antragData').on('click', '.selected', function () {
		var table = $("#antragData").DataTable();
		var data = table.row( this ).data();
		var s_aea_team = data.te_team;
    
    location.href = "detailansicht.php?id="+i_aea_id+"&nr="+s_aea_nr+"&teamname="+s_aea_team;
    return s_aea_nr;
	//$('#antragData').on('click', 'tr', function () {
		//)
	//$('#antragData tbody').on('click', 'tr', function () {}
	//})

	//var data = table.row( this ).data();
	});
});