$(document).ready(function() {
	//Query einlesen
	
	var s_query = "SELECT in VIEW"
  $('#gesamtAEA_data').DataTable( {
      "responsive": true,
	  "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]],
		language: {
            url: "js/json/de.json"
        },  
        "ajax": {
		"url":"serverside/db_select/serverside_gesamtAEA.php",
		//hier wird vom json Array die Anzahl der darin enthaltenen Objekte bestimmt. 
		 "dataSrc": ""},
       
                "columns": [
	                {"data": "aea_id"},
					{"data": "aea_nr"},
                    {"data": "da_deadline"}
				]

        });
    });
