$(document).ready(function() {	
	
	$('#dashboardData').DataTable( {
		"responsive": true,
		"lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]],
		language: {
            url: "js/json/de.json"
        },
		"ajax": {
		"url":"serverside/db_select/serverside_dashboard.php",
		//hier wird vom json Array die Anzahl der darin enthaltenen Objekte bestimmt. 
		 "dataSrc": function (json){
			$("#counter_number").text(json.length);
			return json;
		},
	     // Hier wird der Fehler abgefangen. Die Parameter, die mitgegeben werden sind proprietär von DataTable.
         "error": function (data, xhr, error, code)
			{
				var s_fehler = data.substring(6, 12);
			    var s_url_fehler = data.substring(13);
			    if(s_fehler != "Fehler"){
					var a_dashboard_data = JSON.parse(data);
				
				}else{
				location.href = s_url_fehler;
			    }
                var s_fehler = data.responseText;	
                $("#fehler").innerHTML = s_fehler;	
				console.log(xhr);
				
			}
			
         },	
	     "columns": [
		      {"data": "aea_id"},
			  {"data": "aea_nr"},
			  {"data": "da_deadline"}
		  ],

        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": true,
                "searchable": true
            },
            {
                "targets": [ 2 ],
                "visible": true,
                "searchable": true
            }
            
           ]
	});
	//Weiterleitung
	$(document).on("click", "tr",function(){
		var table = $("#dashboardData").DataTable();
		var data = table.row( this ).data();
		var s_aea_id = data.aea_id;
		var s_aea_nr = data.aea_nr;
    
    location.href = "antragsdaten.php?id="+s_aea_id+"&nr="+s_aea_nr;
    return s_aea_nr;
	});
	

});



