$(document).ready(function() {
//ÄA ID und Teamname aus der URL auslesen
	var s_url = String(window.location);
    if(window.location.search != "") {
//Parameter von der URL trennen
		var a_split = s_url.split("?");
		var a_parameter  = a_split[1].split("&");
		var a_idvalue = a_parameter[0].split("=");
		var i_aea_id = a_idvalue[1];
		var a_nrvalue = a_parameter[1].split("=");
		var s_aea_nr = a_nrvalue[1];
		var a_teamvalue = a_parameter[2].split("=");
		var s_aea_team = a_teamvalue[1];
    }
	var s_aea_nr=$("#AEA_nr").text();
	var s_aea_team=$("#TEAM_name").text();
	
  $('#detail_data').DataTable( {
	    "responsive": true,
        "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]],
        language: {
	         url: "js/json/de.json"
        },
        "ajax": {
            "url": "serverside/db_select/serverside_detaildata.php",
        	"data": {
	  				"AEA-NR": s_aea_nr,
			        "TEAM-NAME": s_aea_team
   			 },
        		"dataSrc": ""
				//Fehlerabfangen
			},
            //hier werden die Spaltenüberschriften erstellt           
            "columns": [ 
	        	{"data": "da_deadline"},
                {"data": "da_finished"}
             ],
			"columnDefs": [
           	{
				"targets": [ 0 ],
             	"visible": true,
             	"searchable": true},
            {
                "targets": [ 1 ],
                "visible": true,
                "searchable": true
            }
			]
	});
	
	$('#comment_data').DataTable( {
		"responsive": true,
        "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]],
        language: {
	         url: "js/json/de.json"
        },
		"ajax": {
            "url": "serverside/db_select/serverside_commentdata.php",
        	"data": {
	  				"AEA-NR": s_aea_nr,
			        "TEAM-NAME": s_aea_team
   			 },
        		"dataSrc": ""
				//Fehlerabfangen
			},
            //hier werden die Spaltenüberschriften erstellt           
            "columns": [ 
	        	{"data": "co_comment"},
                {"data": "co_todo"}
             ]
		
		});
		
	$('#mail_data').DataTable({
		"responsive": true,
        "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]],
        language: {
	         url: "js/json/de.json"
        },
		"ajax": {
            "url": "serverside/db_select/serverside_mailreminderdata.php",
        	"data": {
	  				"AEA-NR": s_aea_nr,
			        "TEAM-NAME": s_aea_team
   			 },
        		"dataSrc": ""
				//Fehlerabfangen
			},
            //hier werden die Spaltenüberschriften erstellt           
            "columns": [ 
	        	{"data": "da_dateMail"}
             ]
	});
						
  
});