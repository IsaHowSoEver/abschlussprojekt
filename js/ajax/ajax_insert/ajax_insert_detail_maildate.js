$("#modal").on('shown.bs.modal', function () {
	$('#btn_insert_maildate').on('click', function () {
	
//Abfrage der Modal_Daten & der Daten aus den Überschriften
		var d_maildate = document.getElementById("0").value;
		
//Team und ÄA abrufen aus der URL
		var s_url = String(window.location);
	    if(window.location.search != "") {
//Parameter von der URL trennen
			var a_value = new Array();
			
			var a_split = s_url.split("?");
			var a_parameter  = a_split[1].split("&");
			for(i = 0; i < a_parameter.length; i++) {
				a_value[i] = a_parameter[i].split("=");
			}
			var i_aea_id = a_value[0][1];
	    }	
		var s_team = document.getElementById("TEAM_name").innerHTML;	
		
//Deadline aus einer anderen Datatable auslesen
		var table = $("#detail_data").DataTable();
		var data = table.row(0).data();
		var d_date_deadline = data.da_deadline;
		
//Prüfung ob das Feld gefüllt ist!!
		if(d_maildate == "0000-00-00"){
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Bitte füllen sie das Feld aus!",
				html:true
			});
			$('#popover').popover('show');
		}else{	
//Plausiprüfung laden
			var s_maildate = "Mail gesendet";
			$.getScript('js/plausibility.js', function(){});
			var b_maildate_format = plausi_date(d_maildate, s_maildate);
			var b_maildate_past = plausi_date_past(d_maildate, s_maildate);
			var b_maildate_future = plausi_date_future(d_maildate, s_maildate);
			
			if(b_maildate_format == true && b_maildate_past == true && b_maildate_future == true){
				select_insert_date(i_aea_id, s_team, d_maildate, d_date_deadline);
			}else{
				$('#popover').popover({
					title: "Fehler",
					placement: "auto",
					content: "Bitte überprüfen sie ihre Eingaben!",
					html:true
				});
				$('#popover').popover('show');
			}	
		}			 
	});
});

function select_insert_date(i_aea_id, s_team, d_maildate, d_date_deadline){
	$.ajax({
		method: "POST",
  		url: "serverside/db_insert/query_select_insert_detail_maildate.php",
		data:{i_var_aea_id:i_aea_id,
			  s_var_team:s_team},
		})
		.done(function(data) {
			var s_fehler = data.substring(6, 12);
			var s_url_fehler = data.substring(13);
			if(s_fehler != "Fehler"){
				var a_inhalte = JSON.parse(data);	
				var i_id_max_date = a_inhalte[0][0][0];
				var i_id_aea_team = a_inhalte[1][0][0];
				
				insert_date(d_maildate, d_date_deadline, i_id_max_date, i_id_aea_team);
			}else{
				location.href = s_url_fehler;
			}							
		})
		.fail(function(data){
			$.getScript('js/error_forwarding.js', function(){});
			error_forwarding(data);
	});
}



function insert_date(d_maildate, d_date_deadline, i_id_max_date, i_id_aea_team){	
//IDs erhöhren um neue IDs für die inserts zu bekommen	
	i_id_max_date++;	
	

//Ajax Call, der die Inserts ausführt
	$.ajax({
		method: "POST",
  		url: "serverside/db_insert/query_insert_detail_maildate.php",
		data:{s_var_maildate:d_maildate,
			  s_var_date_deadline:d_date_deadline,
			  i_s_var_id_max_date: i_id_max_date,
			  i_s_var_id_aea_team:i_id_aea_team
		},
		})
		.done(function(data) {
			var s_fehler = data.substring(6, 12);
			var s_url_fehler = data.substring(13);
			if(s_fehler != "Fehler"){
				$('#popover').popover({
					title: "Durchführung",
					placement: "auto",
					content: "Ihre Neuanlage wurde erfolgreich durchgeführt.",
					html:true
				});
				$('#popover').popover('show');
							
				$('#detail_data').DataTable().destroy();
				$('#comment_data').DataTable().destroy();	
				$('#mail_data').DataTable().destroy();			
				$.getScript('js/ajax/ajax_select/ajaxCall_detailansicht.js', function(){});
				$('#modal').modal("hide");
			}else{
				location.href = s_url_fehler;
			}							
		})
		.fail(function(data){
			$.getScript('js/error_forwarding.js', function(){});
			error_forwarding(data);
	});	
}