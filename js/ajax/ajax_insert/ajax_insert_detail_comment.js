$("#modal").on('shown.bs.modal', function () {
	$('#btn_insert_comment').on('click', function () {	
//Abfrage der Modal_Daten & der Daten in den Überschriften
		var s_comment = document.getElementById("0").value;
		var s_todo = document.getElementById("1").value;
		
//Team und ÄA abrufen aus der URL
		var s_url = String(window.location);
	    if(window.location.search != "") {
//Parameter von der URL trennen
			var a_value = new Array();
			
			var a_split = s_url.split("?");
			var a_parameter  = a_split[1].split("&");
			for(i = 0; i < a_parameter.length; i++) {
				a_value[i] = a_parameter[i].split("=");
			}
			var i_aea_id = a_value[0][1];
	    }	
		var s_team = document.getElementById("TEAM_name").innerHTML;	
		
//Prüfung ob mindestens ein Feld gefüllt ist!!
		if(s_comment == "" && s_todo == ""){
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Mindestens ein Feld muss ausgefüllt sein!",
				html:true
			});
			$('#popover').popover('show');
		}else{		
//Plausiprüfung laden
			$.getScript('js/plausibility.js', function(){});
			var b_xss_detail = xss_script_detail(s_comment, s_todo);
			
			var b_comment_length = plausi_length_comment(s_comment,s_todo);
			
			if(b_xss_detail == true && b_comment_length == true){
				select_insert_comment(i_aea_id, s_team, s_comment, s_todo);
			}else{
				$('#popover').popover({
					title: "Fehler",
					placement: "auto",
					content: "Bitte überprüfen sie ihre Eingaben!",
					html:true
				});
				$('#popover').popover('show');
			}
		}			 
	});
});

function select_insert_comment(i_aea_id, s_team, s_comment, s_todo){
	$.ajax({
		method: "POST",
  		url: "serverside/db_insert/query_select_insert_detail_comment.php",
		data:{i_var_aea_id:i_aea_id,
			  s_var_team:s_team},
		})
		.done(function(data) {
			var s_fehler = data.substring(6, 12);
			var s_url_fehler = data.substring(13);
			if(s_fehler != "Fehler"){
				var a_inhalte = JSON.parse(data);	
				var i_id_max_comment = a_inhalte[0][0][0];
				var i_id_aea_team = a_inhalte[1][0][0];

				insert_comment(s_comment, s_todo, i_id_max_comment, i_id_aea_team);
			}else{
				location.href = s_url_fehler;
			}							
		})
		.fail(function(data){
			$.getScript('js/error_forwarding.js', function(){});
			error_forwarding(data);
	});
}



function insert_comment(s_comment, s_todo, i_id_max_comment, i_id_aea_team){
//IDs erhöhren um neue IDs für die inserts zu bekommen	
	i_id_max_comment++;		

//Ajax Call, der die Inserts ausführt
	$.ajax({
		method: "POST",
  		url: "serverside/db_insert/query_insert_detail_comment.php",
		data:{s_var_comment:s_comment,
			  s_var_todo:s_todo,
			  i_s_var_id_max_comment: i_id_max_comment,
			  i_s_var_id_aea_team:i_id_aea_team
		},
		})
		.done(function(data) {
			var s_fehler = data.substring(6, 12);
			var s_url_fehler = data.substring(13);
			if(s_fehler != "Fehler"){
				$('#popover').popover({
					title: "Durchführung",
					placement: "auto",
					content: "Ihre Neuanlage wurde erfolgreich durchgeführt.",
					html:true
				});
				$('#popover').popover('show');
							
				$('#detail_data').DataTable().destroy();
				$('#comment_data').DataTable().destroy();	
				$('#mail_data').DataTable().destroy();		
				$.getScript('js/ajax/ajax_select/ajaxCall_detailansicht.js', function(){});
				$('#modal').modal("hide");
			}else{
				location.href = s_url_fehler;
			}						
		})
		.fail(function(data){
			$.getScript('js/error_forwarding.js', function(){});
			error_forwarding(data);
	});	
}