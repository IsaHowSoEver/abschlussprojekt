$("#modal").on('shown.bs.modal', function () {
	$('#btn_insert').on('click', function () {
//ÄA Nr aus der URL auslesen
		var s_url = String(window.location);
	    if(window.location.search != "") {
//Parameter von der URL trennen
			var a_split = s_url.split("?");
			var a_parameter  = a_split[1].split("&");
			var a_value_aea_id = a_parameter[0].split("=");
			var i_aea_id = a_value_aea_id[1];
		}
		
//Abfrage der Modal_Daten
		var s_team = document.getElementById("0").value;		
		var s_mail= document.getElementById("1").value;
		var s_comment = document.getElementById("2").value;
		var s_todo = document.getElementById("3").value;
		var d_maildate = document.getElementById("4").value;
		var d_deadline =  document.getElementById("5").value;
		var d_finished = document.getElementById("6").value;	
		
//Prüfung ob die Felder gefüllt sind!!
		if(s_team == "" || s_mail == "" || d_deadline == ""){
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Bitte füllen sie die Pflichtfelder aus!",
				html:true
			});
			$('#popover').popover('show');
		}else{
//Plausiprüfung laden
		$.getScript('js/plausibility.js', function(){});
			var s_maildate ="Mail gesendet";
			var s_deadline = "Termin";
			var s_finished = "Erledigt";
			
			var b_team_length = plausi_length_team_new_entry(s_team, s_mail, s_comment, s_todo);
			
			var b_xss_team = xss_script_team(s_team, s_mail, s_comment, s_todo);
			var b_mail_plausi = mail_plausi(s_mail);
			
			var b_deadline_format = plausi_date(d_deadline, s_deadline);
			var b_deadline_past = plausi_date_past(d_deadline, s_deadline)
			
			if(d_finished != ""){
				var b_finished_format = plausi_date(d_finished, s_finished);
				var b_finished_past = plausi_date_past(d_finished, s_finished);	
				var b_finished_future = plausi_date_future(d_finished, s_finished);				
			}else{
				var b_finished_format = true;
				var b_finished_past = true;
				var b_finished_future = true;
			}
			
			if(d_maildate != ""){	
				var b_maildate_format = plausi_date(d_maildate, s_maildate);
				var b_maildate_past = plausi_date_past(d_maildate, s_maildate);
				var b_maildate_future = plausi_date_future(d_maildate, s_maildate);		
			}else{
				var b_maildate_format = true;
				var b_maildate_past = true;
				var b_maildate_future = true;
			}
			
			if(b_xss_team == true && b_mail_plausi == true && b_team_length == true && b_deadline_format == true && b_deadline_past == true && b_finished_format == true && b_finished_past == true && b_finished_future == true && b_maildate_format == true && b_maildate_past == true && b_maildate_future == true){
				select_insert_team(i_aea_id, s_team, s_mail, s_comment, s_todo, d_maildate, d_deadline, d_finished);
			}else{
				$('#popover').popover({
					title: "Fehler",
					placement: "auto",
					content: "Bitte überprüfen sie ihre Eingaben!",
					html:true
				});
				$('#popover').popover('show');
			}
		}			 
	});
});

function select_insert_team(i_aea_id, s_team, s_mail, s_comment, s_todo, d_maildate, d_deadline, d_finished){	
	$.ajax({
		method: "POST",
  		url: "serverside/db_insert/query_select_insert_team.php",
		data:{s_var_team:s_team},
		})
		.done(function(data) {
			var s_fehler = data.substring(6, 12);
			var s_url_fehler = data.substring(13);
			if(s_fehler != "Fehler"){
				var a_inhalte = JSON.parse(data);
				var i_team = a_inhalte[0];
				
				var i_id_max_team = a_inhalte[1][0][0];			
				var i_id_aea_team = a_inhalte[1][0][1];			
				var i_id_max_comment = a_inhalte[1][0][2];
				var i_id_max_date = a_inhalte[1][0][3];
	
				var i_id_team = a_inhalte[2][0][0];

//Prüfung, ob Team schon vorhanden sind 1=vorhanden 0 =nicht vorhanden
				if(typeof i_team === 'undefined' || i_team == ""){
					window.sessionStorage.setItem("team", "0");	
				}else{
					window.sessionStorage.setItem("team", "1");
				}
				
				insert_team(i_aea_id, s_team, s_mail, s_comment, s_todo, d_maildate, d_deadline, d_finished, i_id_team, i_id_aea_team, i_id_max_team, i_id_max_comment, i_id_max_date);
			}else{
				location.href = s_url_fehler;
			}					
		})
		.fail(function(data){
			$.getScript('js/error_forwarding.js', function(){});
			error_forwarding(data);
	});
}



function insert_team(i_aea_id, s_team, s_mail, s_comment, s_todo, d_maildate, d_deadline, d_finished, i_id_team, i_id_aea_team, i_id_max_team, i_id_max_comment, i_id_max_date){
//Team aus den Session Variablen holen
	var i_team = sessionStorage.getItem('team');

//IDs erhöhren um neue IDs für die inserts zu bekommen
	i_id_max_team++;		
	i_id_max_comment++;	
	i_id_max_date++;
	
	i_id_aea_team++;
	

//Ajax Call, der die Inserts ausführt
	$.ajax({
		method: "POST",
  		url: "serverside/db_insert/query_insert_team.php",
		data:{s_var_team:s_team,
			  s_var_mail:s_mail,
			  s_var_comment:s_comment,
			  s_var_todo:s_todo,
			  s_var_maildate:d_maildate,
			  s_var_deadline:d_deadline,
			  s_var_finished:d_finished,
			  i_var_team:i_team,
			  i_var_aea_id:i_aea_id,
			  i_s_var_id_team:i_id_team,
			  i_s_var_id_max_team:i_id_max_team,
			  i_s_var_id_max_comment:i_id_max_comment,
			  i_s_var_id_max_date:i_id_max_date,
			  i_s_var_id_aea_team:i_id_aea_team
		},
		})
		.done(function(data) {
			var s_fehler = data.substring(6, 12);
			var s_url_fehler = data.substring(13);
			if(s_fehler != "Fehler"){
				$('#popover').popover({
					title: "Durchführung",
					placement: "auto",
					content: "Ihre Neuanlage wurde erfolgreich durchgeführt.",
					html:true
				});
				$('#popover').popover('show');
				
				$('#antragData').DataTable().destroy();		
				$.getScript('js/ajax/ajax_select/ajaxCall_antragsdaten.js', function(){});
				$('#modal').modal("hide");
			}else{
				location.href = s_url_fehler;
			}				
		})
		.fail(function(data){
			$.getScript('js/error_forwarding.js', function(){});
			error_forwarding(data);
	});	
}