$( document ).ready(function(){
	$("#btncomment").hover( function(){
		$('#popover').popover({
			title: "Information",
			placement: "auto",
			content: "Zum Bearbeiten eines Eintrages klicken sie bitte einen Eintrag an. <br> Für eine Neuanlage darf kein Eintrag ausgewählt sein.",
			html:true
		});
		$('#popover').popover('show');
	});
	$("#btncomment").mouseleave(function(){
		$('#popover').popover('dispose');
	});
	
	
	$("#btnmaildate").hover( function(){
		$('#popover').popover({
			title: "Information",
			placement: "auto",
			content: "Zum Bearbeiten eines Eintrages klicken sie bitte einen Eintrag an. <br> Für eine Neuanlage darf kein Eintrag ausgewählt sein.",
			html:true
		});
		$('#popover').popover('show');
	});
	$("#btnmaildate").mouseleave(function(){
		$('#popover').popover('dispose');
	});
	
	
	$("#btndates").hover( function(){
//Prüfung, ob Datensatz ausgewählt wurde	
	 var datatable = $('#detail_data').DataTable();
	 var dataSelected = datatable.rows('.selected').data();

	 if(dataSelected[0] == null) {
		$('#popover').popover({
			title: "Information",
			placement: "auto",
			content: "Zum Bearbeiten eines Eintrages klicken sie bitte einen Eintrag an.",
			html:true
		});
		$('#popover').popover('show');
	}
	});
	$("#btndates").mouseleave(function(){
		$('#popover').popover('dispose');
	});
});