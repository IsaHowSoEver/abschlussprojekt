//Prüfung nach XSS
function xss_script_start(aea_nr, team, mail, comment, todo){
	var patt1 = /</g;
	var patt2 = />/g;
	
 	if(aea_nr.match(patt1) && aea_nr.match(patt2)){
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Es darf kein '<' oder '>' verwendet werden!",
			html:true
		});
		$('#popover').popover('show');
		return false;
	}else if(team.match(patt1) && team.match(patt2)){
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Es darf kein '<' oder '>' verwendet werden!",
			html:true
		});
		$('#popover').popover('show');
		return false;
	}else if(mail.match(patt1) && mail.match(patt2)){
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Es darf kein '<' oder '>' verwendet werden!",
			html:true
		});
		$('#popover').popover('show');
		return false;	
	}else if(comment.match(patt1) && comment.match(patt2)){
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Es darf kein '<' oder '>' verwendet werden!",
			html:true
		});
		$('#popover').popover('show');
		return false;	
	}else if(todo.match(patt1) && todo.match(patt2)){
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Es darf kein '<' oder '>' verwendet werden!",
			html:true
		});
		$('#popover').popover('show');
		return false;	
	}else{
		return true;
	}
}

function xss_script_team(team, mail, comment, todo){
	var patt1 = /</g;
	var patt2 = />/g;
 	if(team.match(patt1) && team.match(patt2)){
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Es darf kein '<' oder '>' verwendet werden!",
			html:true
		});
		$('#popover').popover('show');
		return false;
	}else if(mail.match(patt1) && mail.match(patt2)){
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Es darf kein '<' oder '>' verwendet werden!",
			html:true
		});
		$('#popover').popover('show');
		return false;
	}else if(comment.match(patt1) && comment.match(patt2)){
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Es darf kein '<' oder '>' verwendet werden!",
			html:true
		});
		$('#popover').popover('show');
		return false;
	}else if(todo.match(patt1) && todo.match(patt2)){
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Es darf kein '<' oder '>' verwendet werden!",
			html:true
		});
		$('#popover').popover('show');
		return false;
	}else{
		return true;
	}
}

function xss_script_detail(comment, todo){
	var patt1 = /</g;
	var patt2 = />/g;
 	if(comment.match(patt1) && comment.match(patt2)){
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Es darf kein '<' oder '>' verwendet werden!",
			html:true
		});
		$('#popover').popover('show');
		return false;	
	}else if(todo.match(patt1) && todo.match(patt2)){
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Es darf kein '<' oder '>' verwendet werden!",
			html:true
		});
		$('#popover').popover('show');
		return false;
	}else{
		return true;
	}
}

function xss_script_update(data){
	var patt1 = /</g;
	var patt2 = />/g;
 	if(data.match(patt1) && data.match(patt2)){
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Es darf kein '<' oder '>' verwendet werden!",
			html:true
		});
		$('#popover').popover('show');
		return false;
	}else{
		return true;
	}
}

function mail_plausi(data){
	var patt = /@/g;
 	if(data.match(patt)){
		return true;
	}else{
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Eine Mail-Adresse hat immer ein @!",
			html:true
		});
		$('#popover').popover('show');
		return false;
	}
}

//Plausiprüfungen, dass die Längen nicht die maximale Längen überschreitet
function plausi_length_aea_new_entry(aea_nr, team, mail, comment, todo){	
	var Spalten;
	var aea_length = aea_nr.length;	
	var team_length = team.length;
	var mail_length = mail.length;
	var comment_length = comment.length;
	var todo_length = todo.length;	
	
	if(aea_length > 255 || team.length > 255 || mail.length > 255 || comment.length > 255 || todo.length > 255){
		if(aea_length > 255){
			Spalten = "ÄA-Nr";
		}else if(team_length > 255){
			Spalten = "Realisierungsteam";
		}else if(mail_length > 255 ){
			Spalten = "E-Mail";
		}else if(comment_length > 255){
			Spalten = "Kommentar";
		}else if(todo_length > 255){
			Spalten = "Zu erledigen";
		}
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Zu viele Zeichen! Bitte kürzen sie den Inhalt: "+Spalten,
			html:true
		});
		$('#popover').popover('show');
		return false;
	}else{
		return true;
	}
}

function plausi_length_aea_edit_entry(id, aea_nr){	
	var Spalten;
	var id_length = id.length;
	var aea_length = aea_nr.length;	
	
	if(id_length > 11 || aea_length > 255){
		if(id_length > 11){
			Spalten = "ID";
		}else if(aea_length > 255){
			Spalten = "ÄA-Nr";
		}
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Zu viele Zeichen! Bitte kürzen sie den Inhalt: "+Spalten,
			html:true
		});
		$('#popover').popover('show');
		return false;
	}else{
		return true;
	}
}  
    
function plausi_length_team_new_entry(team, mail, comment, todo){
	var Spalten;	
	var team_length = team.length;
	var mail_length = mail.length;
	var comment_length = comment.length;
	var todo_length = todo.length;

	if(team_length > 255 || mail_length > 255 || comment_length > 255 || todo_length > 255){
		if(team_length > 255){
			Spalten = "Realisierungsteam";
		}else if(mail_length > 255 ){
			Spalten = "E-Mail";
		}else if(comment_length > 255){
			Spalten = "Kommentar";
		}else if(todo_length > 255){
			Spalten = "Zu erledigen";
		}
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Zu viele Zeichen! Bitte kürzen sie den Inhalt: "+Spalten,
			html:true
		});
		$('#popover').popover('show');
		return false;
	}else{
		return true;
	}
}
 
function plausi_length_team_edit_entry(team, mail){
	var Spalten;	
	var team_length = team.length;
	var mail_length = mail.length;

	if(team_length > 255 || mail_length > 255){
		if(team_length > 255){
			Spalten = "Realisierungsteam";
		}else if(mail_length > 255 ){
			Spalten = "E-Mail";
		}
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Zu viele Zeichen! Bitte kürzen sie den Inhalt: "+Spalten,
			html:true
		});
		$('#popover').popover('show');
		return false;
	}else{
		return true;
	}
}
 
function plausi_length_comment(comment, todo){
	var Spalten;	
	var comment_length = comment.length;
	var todo_length = todo.length;

	if(comment_length > 255 || todo_length > 255){
		if(comment_length > 255){
			Spalten = "Kommentar";
		}else if(todo_length > 255){
			Spalten = "Zu erledigen";
		}
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Zu viele Zeichen! Bitte kürzen sie den Inhalt: "+Spalten,
			html:true
		});
		$('#popover').popover('show');
		return false;
	}else{
		return true;
	}
} 
 

function plausi_date(date, s_date){
//Prüfung ob das Datum aus Zaheln besteht
	var a_date = date.split("-");
	var year = parseInt(a_date[0], 10);
	var month = parseInt(a_date[1], 10);
	var day = parseInt(a_date[2], 10);

	if(isNaN(year)){	
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Bitte nur Zahlen eingeben!",
			html:true
		});
		$('#popover').popover('show');
          return false;
    }else if(isNaN(month)){	
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Bitte nur Zahlen eingeben!",
			html:true
		});
		$('#popover').popover('show');
          return false;
	}else if(isNaN(day)){	
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Bitte nur Zahlen eingeben!",
			html:true
		});
		$('#popover').popover('show');
          return false;
	}

//Pürfung ob das Format eingehalten wurde	
	if(year.toString().length != 4 ){	
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Das Jahr von  '"+s_date+"' muss 4-Stellig sein!",
			html:true
		});
		$('#popover').popover('show');
        return false;
    }else if(month > 12 && month <= 0){	
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Der Monat ihm Jahr von  '"+s_date+"' fängt bei 1 an!",
			html:true
		});
		$('#popover').popover('show');
        return false;
	}else if(month.toString().length < 1 || month.toString().length > 2){	
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Der Monat von '"+s_date+"' ist ein- bis max. zweistellig!",
			html:true
		});
		$('#popover').popover('show');
        return false;
	}else if(day.toString().length < 1 || day.toString().length > 2){	
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Der Tag von '"+s_date+"' ist ein- bis max. zweistellig!",
			html:true
		});
		$('#popover').popover('show');
        return false;
	}else if(day > 31 && day <= 0){	
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Es gibt nur max. 31 Tage im Monat von '"+s_date+"'  und fängt bei Tag 1 an!",
			html:true
		});
		$('#popover').popover('show');
        return false;
	}else{
		return true;
	}
}  

function plausi_date_past(date, s_date){
//Eingegebenes Datum in Einzelteile splitten 
	var a_date = date.split("-");
	var year = parseInt(a_date[0], 10);
	var month = parseInt(a_date[1], 10);
	var day = parseInt(a_date[2], 10);
	
//Aktuelles Datum speichern
	var date_current_time = new Date();
	var date_current_year = date_current_time.getFullYear();
	var date_current_month = date_current_time.getMonth() + 1;
	var date_current_day = date_current_time.getDate();
	
		
//Prüfen ob das Datum mehr als ein Jahr zurück liegt
	if(year < date_current_year){
		if(day < date_current_day && month <= date_current_month){	
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Das Datum '"+s_date+"' darf nicht älter als ein Jahr sein!",
				html:true
			});
			$('#popover').popover('show');
			return false;
		}else if(day > date_current_day && month < date_current_month){		
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Das Datum '"+s_date+"' darf nicht älter als ein Jahr sein!",
				html:true
			});		
			$('#popover').popover('show');
			return false;
		}else if(day == date_current_day && month < date_current_month){	
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Das Datum '"+s_date+"' darf nicht älter als ein Jahr sein!",
				html:true
			});		
			$('#popover').popover('show');
			return false;
		}else{
			return true;
		}
	}else{
		return true;
	}
}

function plausi_date_future(date, s_date){
//Eingegebenes Datum in Einzelteile splitten 
	var a_date = date.split("-");
	var year = parseInt(a_date[0], 10);
	var month = parseInt(a_date[1], 10);
	var day = parseInt(a_date[2], 10);
	
//Aktuelles Datum speichern
	var date_current_time = new Date();
	var date_current_year = date_current_time.getFullYear();
	var date_current_month = date_current_time.getMonth() + 1;
	var date_current_day = date_current_time.getDate();
	
//Prüfen ob das Datum nicht in der Zukunft liegt (Tag <, >, =; Monat <, >, = & Jahr >)	
		if(day > date_current_day && month >= date_current_month && year >= date_current_year){		
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Das Datum '"+s_date+"' kann nicht in der Zukunft liegen!",
				html:true
			});	
			$('#popover').popover('show');
			return false;
		}else if(day < date_current_day && month >= date_current_month && year > date_current_year){	
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Das Datum '"+s_date+"' kann nicht in der Zukunft liegen!",
				html:true
			});			
			$('#popover').popover('show');
			return false;
		}else if(day < date_current_day && month > date_current_month && year >= date_current_year){	
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Das Datum '"+s_date+"' kann nicht in der Zukunft liegen!",
				html:true
			});			
			$('#popover').popover('show');
			return false;
		}else if(day == date_current_day && month > date_current_month && year >= date_current_year){		
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Das Datum '"+s_date+"' kann nicht in der Zukunft liegen!",
				html:true
			});			
			$('#popover').popover('show');
			return false;				
		}else if(day == date_current_day && year > date_current_year){		
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Das Datum '"+s_date+"' kann nicht in der Zukunft liegen!",
				html:true
			});					
			$('#popover').popover('show');
			return false;	
		}else if(month < date_current_month && year > date_current_year){	
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Das Datum '"+s_date+"' kann nicht in der Zukunft liegen!",
				html:true
			});					
			$('#popover').popover('show');
			return false;	
		}else if(month > date_current_month && year >= date_current_year){		
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Das Datum '"+s_date+"' kann nicht in der Zukunft liegen!",
				html:true
			});				
			$('#popover').popover('show');
			return false;	
		}else if(month == date_current_month && day > date_current_day && year >= date_current_year){	
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Das Datum '"+s_date+"' kann nicht in der Zukunft liegen!",
				html:true
			});				
			$('#popover').popover('show');
			return false;				
		}else if(month == date_current_month && day <= date_current_day && year > date_current_year){		
			$('#popover').popover({
				title: "Fehler",
				placement: "auto",
				content: "Das Datum '"+s_date+"' kann nicht in der Zukunft liegen!",
				html:true
			});			
			$('#popover').popover('show');
			return false;								
		}else{
			return true;
		}
}
           