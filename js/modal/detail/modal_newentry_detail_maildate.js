$('#btnmaildate').on('click', function () { 
	
//Prüfung, ob Datensatz ausgewählt wurde
	var datatable = $('#mail_data').DataTable();
	var dateSelected = datatable.rows('.selected').data();
	if(dateSelected.da_dateMail == null) {
	
//Deklaration der Spaltennamen für die Input Felder	und der Button vom Modal
		var s_footer = "<div><p>Mit * gekennzeichnete Felder sind Pflichtfelder</p></div>"+
					   "<div> <button class='button_special' id='btn_insert_maildate'><b>Speichern</b></a>"+
				       "<button class='button_special' data-dismiss='modal'><b>Schließen</b></button></div>";

//Deklaration des Input Feldes des Modals		
		var s_html_tag = "<label class='ml-3' for='maildate'>Mail gesendet:*</label><br>"+
						 "<div class='input-group date' id='datepicker1' data-date-format='yyyy-mm-dd'>"+
						 "<input id='0' class='form_control container-fluid-own mb-3 ml-3 datepicker' size='16' type='text' placeholder='yyyy-mm-dd'>"+
						 "</div>"; 
		
//Modal im HTML füllen
		document.getElementById('title').innerHTML = "Neuer Eintrag";
		document.getElementById('modal_body').innerHTML = s_html_tag;
		document.getElementById('modal_footer').innerHTML = s_footer;
		
		$("#modal").modal('show');
	}
});	