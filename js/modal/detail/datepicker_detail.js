$("#modal").on('shown.bs.modal', function () {
	$('#datepicker1').on('click', function(){
		$('#datepicker1').datepicker({
	    	startDate: '-1y',
			todayHighlight: true,
			language: "de"
		});			
	});
	
	$('#datepicker2').on('click', function(){
		$('#datepicker2').datepicker({
	    	startDate: '-1y',
			todayHighlight: true,
			language: "de"
		});			
	});
		
});
$('#modal').on('change', function(){
	$('#datepicker1').datepicker( "destroy" );
	$('#datepicker2').datepicker( "destroy" );
});	