$('#btncomment').on('click', function () {	
	
//Prüfung, ob Datensatz ausgewählt wurde
	 var datatable = $('#comment_data').DataTable();
	 var dataSelected = datatable.rows('.selected').data();

	 if(dataSelected[0] != null) {
		
		var s_co_comment = dataSelected[0].co_comment;
		var s_co_todo = dataSelected[0].co_todo;
		window.sessionStorage.setItem("comment", s_co_comment);
		window.sessionStorage.setItem("todo", s_co_todo);
		
		var s_footer = "<div> <button class='button_special' id='btn_update_comment'><b>Speichern</b></a>"+
				  	   "<button class='button_special' data-dismiss='modal'><b>Schließen</b></button></div>";

//Deklaration der Textarea Felder des Modals			
		var s_html_tag  = "<label class='ml-3' for='0'>Kommentar:</label><br>"+
						  "<textarea  id='0' class='form_control container-fluid-own mb-3 ml-3'>" + s_co_comment + "</textarea><br>"+
						  "<label class='ml-3' for='1'>Zu erledigen:</label><br>"+
						  "<textarea id='1' class='form_control container-fluid-own mb-3 ml-3'>" + s_co_todo + "</textarea>";				
		
//Modal im HTML füllen
		document.getElementById('title').innerHTML = "Änderung vornehmen";	
		document.getElementById('modal_body').innerHTML = s_html_tag;
		document.getElementById('modal_footer').innerHTML = s_footer;
		$("#modal").modal('show');
	}
});