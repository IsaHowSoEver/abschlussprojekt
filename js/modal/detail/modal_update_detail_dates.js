$('#btndates').on('click', function () {
	
//Prüfung, ob Datensatz ausgewählt wurde	
	var datatable = $('#detail_data').DataTable();
	var dataSelected = datatable.rows('.selected').data();

	if(dataSelected[0] != null) {
		var s_deadline = dataSelected[0].da_deadline;
		var s_finished = dataSelected[0].da_finished;
		window.sessionStorage.setItem("deadline", s_deadline);
		
		var s_footer = "<div><p>Mit * gekennzeichnete Felder sind Pflichtfelder</p></div>"+
				       "<div> <button class='button_special' id='btn_update_dates'><b>Speichern</b></a>"+
				  	   "<button class='button_special' data-dismiss='modal'><b>Schließen</b></button></div>";

//Deklaration der Input Felder des Modals		
		var s_html_tag = "<label class='ml-3' for='0'>Termin:*</label><br>"+
						 "<div class='input-group date' id='datepicker1' data-date-format='yyyy-mm-dd'>"+
						 "<input id='0' class='form_control container-fluid-own mb-3 ml-3 datepicker' size='16' type='text' value='" + s_deadline + "' placeholder='yyyy-mm-dd'>"+
						 "</div><br>"+
						 "<label class='ml-3' for='1'>Erledigt:</label><br>"+
						 "<div class='input-group date' id='datepicker2' data-date-format='yyyy-mm-dd'>"+
						 "<input id='1' class='form_control container-fluid-own mb-3 ml-3 datepicker' size='16' type='text' value='" + s_finished + "' placeholder='yyyy-mm-dd'>"+
						 "</div><br>";			
		
		//Modal im HTML füllen
		document.getElementById('title').innerHTML = "Änderung vornehmen";
		document.getElementById('modal_body').innerHTML = s_html_tag;
		document.getElementById('modal_footer').innerHTML = s_footer;
		$("#modal").modal('show');
	}
});