$('#btncomment').on('click', function () {	
	
//Prüfung, ob Datensatz ausgewählt wurde
	 var datatable = $('#comment_data').DataTable();
	 var dataSelected = datatable.rows('.selected').data();

	 if(dataSelected.co_comment == null && dataSelected.co_todo == null) {		
//Deklaration der Spaltennamen für die Textarea Felder und der Button vom Modal
		var a_spalten = ["Kommentar", "Zu erledigen"];
		var s_footer = "<div> <button class='button_special' id='btn_insert_comment'><b>Speichern</b></a>"+
				       "<button class='button_special' data-dismiss='modal'><b>Schließen</b></button></div>";
	
		var a_html_tag = new Array();
//Deklaration der Textarea Felder des Modals			
		for(var i = 0; i <= 1; i++){			
			a_html_tag[i] = "<label class='ml-3' for='"+i+"'>"+a_spalten[i]+":</label><br>"+
							"<textarea id='"+i+"' class='form_control container-fluid-own mb-3 ml-3'></textarea><br>"; 
		}
//Array in String umwandeln um die Abtrennung "," zu umgehen
		var s_html_tag = a_html_tag.join(" ");
		
//Modal im HTML füllen
		document.getElementById('title').innerHTML = "Neuer Eintrag";
		document.getElementById('modal_body').innerHTML = s_html_tag;
		document.getElementById('modal_footer').innerHTML = s_footer;
		$("#modal").modal('show');
	}
});	
