$('#editentry').on('click', function () {
//Selektierte Daten speichern
	var datatable = $('#antragData').DataTable();
	var dataSelected = datatable.rows('.selected').data();
	
//Prüfung, ob Datensatz ausgewählt wurde
	if(dataSelected[0] != null) {
			
	var s_team = dataSelected[0].te_team;
	var s_mail = dataSelected[0].te_mail;
	window.sessionStorage.setItem("team", s_team);
	window.sessionStorage.setItem("mail", s_mail);
		
	var s_footer = "<div><p>Mit * gekennzeichnete Felder sind Pflichtfelder</p></div>"+
				   "<div><button class='button_special' id='btn_update'><b>Speichern</b></a>"+
				   "<button class='button_special' data-dismiss='modal'><b>Schließen</b></button></div>";

//Deklaration der Input Felder des Modals		
	var s_html_tag = "<label class='ml-3' for='0'>Team:*</label><br>"+
					 "<input list='list' id='0' name='team' class='form_control container-fluid-own mb-3 ml-3' value='" + s_team + "'>"+
					 "<datalist id='list'></datalist><br>"+
					 "<label class='ml-3' for='1'>Mail:*</label><br>"+
					 "<input id='1' name='mail' class='form_control container-fluid-own mb-3 ml-3' value='" + s_mail + "'><br>";					
		
//Modal im HTML füllen
	document.getElementById('title').innerHTML = "Änderung vornehmen";
	document.getElementById('modal_body').innerHTML = s_html_tag;
	document.getElementById('modal_footer').innerHTML = s_footer;
	$("#modal").modal('show');
	
	}else{
		$('#popover').popover({
			title: "Fehler",
			placement: "auto",
			content: "Sie müssen vorher einen Eintrag auswählen!",
			html:true
		});
		$('#popover').popover('show');
	}
});

