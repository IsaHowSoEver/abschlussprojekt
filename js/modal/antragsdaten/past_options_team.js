$( document ).ready(function(){
	$('#modal').on('shown.bs.modal', function () {
//Array für die URL Parameter deklarieren
		var a_value = new Array();
//ÄA Nr aus der URL auslesen
		var s_url = String(window.location);
    	if(window.location.search != "") {
//Parameter von der URL trennen
			var a_split = s_url.split("?");
			var a_parameter  = a_split[1].split("&");
			for(i = 0; i < a_parameter.length; i++) {
				a_value[i] = a_parameter[i].split("=");
			}
			var i_aea_id = a_value[0][1];
    	}
		select_options_team(i_aea_id);	
		$("input[name='team']").change(function(){
			select_mail_team();
		});
	});	
});


function select_options_team(i_aea_id){
	$.ajax({
		method: "POST",
  		url: "serverside/db_select/query_select_options_team.php",
		data:{i_var_aea_id:i_aea_id},
		})
		.done(function(data) {
			var s_fehler = data.substring(6, 12);
			var s_url_fehler = data.substring(13);
			if(s_fehler != "Fehler"){
				var a_inhalte = JSON.parse(data);
				
				var a_datalist = new Array();

				for(i=0; i < a_inhalte.length; i++){
					var s_team = a_inhalte[i];
					a_datalist[i] = "<option value='"+s_team+"'></option>";
				}
				$("datalist").append(a_datalist);
			}else{
				location.href = s_url_fehler;
			}	
			})
		.fail(function(data){
			$.getScript('js/error_forwarding.js', function(){});
			error_forwarding(data);
	});	
}

function select_mail_team(){
	var s_team = document.getElementById("0").value;
	$.ajax({
		method: "POST",
  		url: "serverside/db_select/query_select_mail_team.php",
		data:{s_var_team:s_team},
		})
		.done(function(data) {
			var s_fehler = data.substring(6, 12);
			var s_url_fehler = data.substring(13);
			if(s_fehler != "Fehler"){
				var a_inhalte = JSON.parse(data);
				if(a_inhalte!= ""){
					var s_mail = a_inhalte[0][0];
					$("input[name='mail']").val(s_mail);
				}
			}else{
				location.href = s_url_fehler;
			}	
			})
		.fail(function(data){
			$.getScript('js/error_forwarding.js', function(){});
			error_forwarding(data);
	});	
};

