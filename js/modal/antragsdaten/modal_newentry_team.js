$('#newentry').on('click', function () {	
	
//Deklaration der Spaltennamen für die Input/Textarea Felder und der Button vom Modal
	var a_spalten = ["Führendes Realisierungsteam", "E-Mail", "Kommentar", "Zu erledigen", "Mail gesendet", "Termin", "Erledigt"];
	var s_footer = "<div><p>Mit * gekennzeichnete Felder sind Pflichtfelder</p></div>"+
				   "<div> <button class='button_special' id='btn_insert'><b>Speichern</b></a>"+
				   "<button class='button_special' data-dismiss='modal'><b>Schließen</b></button></div>";

	var a_html_tag = new Array();

//Deklaration der Input/Textarea Felder des Modals				
	for(var i = 0; i <= 6; i++){				
		if(i == 0){
			a_html_tag[i] = "<label class='ml-3' for='"+i+"'>"+a_spalten[i]+"*:</label><br>"+
							"<input list='list' id='"+i+"' name='team' class='form_control container-fluid-own mb-3 ml-3'><br><br>"+
							"<datalist id='list'></datalist>"; 
		}else if(i == 1){
			a_html_tag[i] = "<label class='ml-3' for='"+i+"'>"+a_spalten[i]+"*:</label><br>"+
							"<input id='"+i+"' name='mail' class='form_control container-fluid-own mb-3 ml-3'><br>";
		}else if(i >= 2 && i <= 3){
			a_html_tag[i] = "<label class='ml-3' for='"+i+"'>"+a_spalten[i]+":</label><br>"+
							"<textarea id='"+i+"' class='form_control container-fluid-own mb-3 ml-3'>"+
							"</textarea><br>"; 
		}else if(i == 5){
			a_html_tag[i] = "<label class='ml-3' for='"+i+"'>"+a_spalten[i]+"*:</label><br>"+
							"<div class='input-group date' id='datepicker"+i+"' data-date-format='yyyy-mm-dd'>"+
							"<input id='"+i+"' name='termin' class='form_control container-fluid-own mb-3 ml-3 datepicker' size='16' type='text' placeholder='yyyy-mm-dd'>"+
						 	"</div><br>";	
		}else{
			a_html_tag[i] = "<label class='ml-3' for='"+i+"'>"+a_spalten[i]+":</label><br>"+
							"<div class='input-group date' id='datepicker"+i+"' data-date-format='yyyy-mm-dd'>"+
							"<input id='"+i+"' class='form_control container-fluid-own mb-3 ml-3 datepicker' size='16' type='text' placeholder='yyyy-mm-dd'>"+ 
						 	"</div><br>";	
		}
	}
//Array in String umwandeln um die Abtrennung "," zu umgehen
	var s_html_tag = a_html_tag.join(" ");
	
//Modal im HTML füllen
	document.getElementById('title').innerHTML = "Neuer Eintrag";
	document.getElementById('modal_body').innerHTML = s_html_tag;
	document.getElementById('modal_footer').innerHTML = s_footer;
	$("#modal").modal('show');

});	
