$("#modal").on('shown.bs.modal', function () {
	$('#datepicker4').on('click', function(){
		$('#datepicker4').datepicker({
	    	startDate: '-1y',
			todayHighlight: true,
			language: "de"
		});			
	});
	$('#datepicker5').on('click', function(){
		$('#datepicker5').datepicker({
	    	startDate: '-1y',
			todayHighlight: true,
			language: "de"
		});			
	});
	$('#datepicker6').on('click', function(){
		$('#datepicker6').datepicker({
	    	startDate: '-1y',
			todayHighlight: true,
			language: "de"
		});			
	});	
});
$('#modal').on('change', function(){
	$('#datepicker4').datepicker( "destroy" );
	$('#datepicker5').datepicker( "destroy" );
	$('#datepicker6').datepicker( "destroy" );
});	