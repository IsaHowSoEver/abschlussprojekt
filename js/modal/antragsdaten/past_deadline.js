$( document ).ready(function(){
	$('#modal').on('shown.bs.modal', function () {
//Array für die URL Parameter deklarieren
		var a_value = new Array();
//ÄA Nr aus der URL auslesen
		var s_url = String(window.location);
    	if(window.location.search != "") {
//Parameter von der URL trennen
			var a_split = s_url.split("?");
			var a_parameter  = a_split[1].split("&");
			for(i = 0; i < a_parameter.length; i++) {
				a_value[i] = a_parameter[i].split("=");
			}
			var i_aea_id = a_value[0][1];
    	}
		select_deadline(i_aea_id);	
	});	
});


function select_deadline(i_aea_id){
	$.ajax({
		method: "POST",
  		url: "serverside/db_select/query_select_deadline.php",
		data:{i_var_aea_id:i_aea_id},
		})
		.done(function(data) {
			var s_fehler = data.substring(6, 12);
			var s_url_fehler = data.substring(13);
			if(s_fehler != "Fehler"){
				var a_inhalte = JSON.parse(data);
				if(a_inhalte!= ""){
					var d_deadline = a_inhalte[0][0];
					$("input[name='termin']").val(d_deadline);
				}else{
					location.href = s_url_fehler;
				}	
			}else{
				location.href = s_url_fehler;
			}	
			})
		.fail(function(data){
			$.getScript('js/error_forwarding.js', function(){});
			error_forwarding(data);
	});	
}

