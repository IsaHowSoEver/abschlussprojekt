$("#modal").on('shown.bs.modal', function () {
	$('#datepicker5').on('click', function(){
		$('#datepicker5').datepicker({
	    	startDate: '-1y',
			todayHighlight: true,
			language: "de"
		});			
	});
	$('#datepicker6').on('click', function(){
		$('#datepicker6').datepicker({
	    	startDate: '-1y',
			todayHighlight: true,
			language: "de"
		});			
	});
	$('#datepicker7').on('click', function(){
		$('#datepicker7').datepicker({
	    	startDate: '-1y',
			todayHighlight: true,
			language: "de"
		});			
	});	
});
$('#modal').on('change', function(){
	$('#datepicker5').datepicker( "destroy" );
	$('#datepicker6').datepicker( "destroy" );
	$('#datepicker7').datepicker( "destroy" );
});	