$('#dashboard').on('click', 'tr', function () {
	var table = $("#dashboardData").DataTable();
	if ( $(this).hasClass('selected') ) {
    	$(this).removeClass('selected');
    }else {
     	table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
    }
});

$('#antragData').on('click', 'tr', function () {
	var table = $("#antragData").DataTable();
	if ( $(this).hasClass('selected') ) {
    	$(this).removeClass('selected');
    }else {
     	table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
    }
});

$('#detail_data').on('click', 'tr', function () {
	var table = $("#detail_data").DataTable();
	if ( $(this).hasClass('selected') ) {
    	$(this).removeClass('selected');
    }else {
     	table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
    }
});

$('#comment_data').on('click', 'tr', function () {
	var table = $("#comment_data").DataTable();
	if ( $(this).hasClass('selected') ) {
    	$(this).removeClass('selected');
    }else {
     	table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
    }
});

$('#mail_data').on('click', 'tr', function () {
	var table = $("#mail_data").DataTable();
	if ( $(this).hasClass('selected') ) {
    	$(this).removeClass('selected');
    }else {
     	table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
    }
});