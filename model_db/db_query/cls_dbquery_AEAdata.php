<?php

namespace model_db\db_query;

use model_db\db_connect\cls_dbConnect;

class cls_dbquery_AEAdata extends cls_dbConnect
{
	
	private $s_host = "localhost";
	private $s_dataBase = "db_uebaea";
	private $s_dbUser = "root";
	private $s_dbUserPw = "";
	
	
	public $a_AEA_data = array();
	
	public function __construct($s_aea_id)
	{
		parent::__construct($this->s_host, $this->s_dataBase, $this->s_dbUser, $this->s_dbUserPw);
		$this->get_AEA_data($s_aea_id);
	}
	//ID übergeben
	private function get_AEA_data($s_aea_id)
	{
		$o_Result = $this->getO_mysqli();
		
		$s_query = 'SELECT te_team, te_mail FROM tbl_team'.
				' JOIN tbl_aea_team ON tbl_team.te_id = tbl_aea_team.at_id_team'.
				' JOIN tbl_aea ON tbl_aea.aea_id = tbl_aea_team.at_id_aea'.
				' WHERE tbl_aea.aea_id = ?';
		
		$stmt = $o_Result->prepare($s_query);
		$stmt->bind_param('s', $s_aea_id);
		$stmt->execute();
		
		$s_result = $stmt ->get_result();
		
		
		while ($a_dsatz = $s_result -> fetch_assoc()){

			
			$this->a_AEA_data[] = $a_dsatz;
		}
		
		
	}
	/**
	 * @return multitype:
	 */
	public function getA_AEA_data()
	{
		return $this->a_AEA_data;
	}
	
}

