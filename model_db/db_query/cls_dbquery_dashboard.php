<?php

namespace model_db\db_query;

use model_db\db_connect\cls_dbConnect;

class cls_dbquery_dashboard extends cls_dbConnect
{
	
	private $s_host = "localhost";
	private $s_dataBase = "db_uebaea";
	private $s_dbUser = "root";
	private $s_dbUserPw = "";

	
	public $a_dashboard_data = array();
	

	public function __construct()
	{
		parent::__construct($this->s_host, $this->s_dataBase, $this->s_dbUser, $this->s_dbUserPw);
		$this->get_dashboard_data();
	}
	 
	private function get_dashboard_data()
	{
		$o_Result = $this->getO_mysqli();
		$s_query = 'SELECT * FROM view_dashboard';
		$s_result = $o_Result->query($s_query);
		
		while ($a_dsatz = mysqli_fetch_assoc($s_result)) {
			
			$this->a_dashboard_data[] = $a_dsatz;
		}
		
	}
	
	/**
	 * @return multitype:
	 */
	public function getA_dashboard_data()
	{
		return $this->a_dashboard_data;
	}	
	
}
?>