<?php

namespace model_db\db_query;

use model_db\db_connect\cls_dbConnect;

class cls_dbquery_gesamtansicht extends cls_dbConnect{
	
	private $s_host = "localhost";
	private $s_dataBase = "db_uebaea";
	private $s_dbUser = "root";
	private $s_dbUserPw = "";
	
	
	public $a_gesamtansicht_data = array();
	

	public function __construct()
	{
		parent::__construct($this->s_host, $this->s_dataBase, $this->s_dbUser, $this->s_dbUserPw);
		$this->get_gesamtansicht_data();
		
	}
	
	private function get_gesamtansicht_data()
	{
		$o_Result = $this->getO_mysqli();
		$s_query = 'SELECT aea_id, aea_nr, da_deadline FROM tbl_aea JOIN tbl_dates WHERE tbl_aea.aea_id = tbl_dates.da_id';
		$s_result = $o_Result->query($s_query);
		
		while ($a_dsatz = mysqli_fetch_assoc($s_result)) {
			
			$this->a_gesamtansicht_data[] = $a_dsatz;
		}
		
	}
	
	/**
	 * @return multitype:
	 */
	public function getA_gesamtansicht_data()
	{
		return $this->a_gesamtansicht_data;
	}

	
}
?>
