<?php

namespace model_db\db_query;

use Exception;
use model_db\db_connect\cls_dbConnect;
 

class query_select_general extends cls_dbConnect{
	
	private $s_db_host = "localhost";
	private $s_db_user = "root";
	private $s_db_passw = "";
	private $s_db_name = "db_uebaea";
	
	private $a_inhalte = array();
	
	
	public function __construct($s_query, $a_values)
	{
		parent::__construct($this->s_db_host, $this->s_db_name, $this->s_db_user, $this->s_db_passw);
		$o_mysqli = parent::getO_mysqli();
		$this->executeStatement($o_mysqli, $s_query, $a_values);
	}
	
	private function executeStatement($o_mysqli, $s_query, $a_values)
	{
		error_reporting(0);
		try {
			if(!$stmt = $o_mysqli->prepare($s_query)){
				throw new Exception("query");
			}else{
				$count = count($a_values);
				$bindStr = str_repeat('s', $count);
				
				$stmt->bind_param($bindStr, ...$a_values);
				$stmt->execute();
				
				$result = $stmt->get_result();
				$this->buildResultArray($result);
			}
		}catch(Exception $e){
			echo "Fehler:".$this->getS_uriPre()."/ueb-aea/err/err.php?f=".$e->getMessage();
			exit;
		}
	}
	
	
	private function buildResultArray($result)
	{
		$a_erg = array();
		$i_zaehl = 0;
		
		While ($row =$result->fetch_array()){
			$field_count = $result->field_count;
			for($i=0; $i < $field_count ;$i++){
				$a_erg[$i_zaehl][$i]=$row[$i];
				
			}
			$i_zaehl ++;
		}
		$this->a_inhalte = $a_erg;
		
	}
	public function getInhalte()
	{
		return $this->a_inhalte;
	}
}

