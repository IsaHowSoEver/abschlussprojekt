<?php

namespace model_db\db_query;

use Exception;
use model_db\db_connect\cls_dbConnect;

class query_insert_general extends cls_dbConnect{
		
	private $s_db_host = "localhost";
	private $s_db_user = "root";
	private $s_db_passw = "";
	private $s_db_name = "db_uebaea";
		
	public function __construct($s_query, $a_values)
	{
		parent::__construct($this->s_db_host, $this->s_db_name, $this->s_db_user, $this->s_db_passw);
		$o_mysqli = parent::getO_mysqli();
		$this->executeStatement($o_mysqli, $s_query, $a_values);
	}
	
	private function executeStatement($o_mysqli, $s_query, $a_values)
	{
		error_reporting(0);
		try {
			if(!$stmt = $o_mysqli->prepare($s_query)){
				throw new Exception("query");
			}else{
				$count = count($a_values);
				$bindStr = str_repeat('s', $count);
				
				$stmt->bind_param($bindStr, ...$a_values);
				$stmt->execute();
			}
		}catch(Exception $e){
			echo "Fehler:".$this->getS_uriPre()."/ueb-aea/err/err.php?f=".$e->getMessage();
			exit;
		}
	}	
}


