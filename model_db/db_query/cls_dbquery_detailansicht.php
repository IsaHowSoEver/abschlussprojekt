<?php

namespace model_db\db_query;

use model_db\db_connect\cls_dbConnect;

class cls_dbquery_detailansicht extends cls_dbConnect 
{
	
	private $s_host = "localhost";
	private $s_dataBase = "db_uebaea";
	private $s_dbUser = "root";
	private $s_dbUserPw = "";
	
	public $a_detail_data = array();
	public $a_comment_data = array();
	public $a_mailreminder_data = array();


	//Überlegen ob man nicht die Kommentare und Mail-erinnerungen in einzelne Klassen unterteilt
	public function __construct($s_aea_nr, $s_team_name) 
	{
		parent::__construct($this->s_host, $this->s_dataBase, $this->s_dbUser, $this->s_dbUserPw);
		$this->get_detailansicht_data($s_aea_nr, $s_team_name);
		$this->get_comment_data($s_aea_nr, $s_team_name);
		$this->get_mailreminder_data($s_aea_nr, $s_team_name);
		//Funktionsaufruf
	}
	//Antrags-ID und Team-Name wird übergeben
	private function get_detailansicht_data($s_aea_nr, $s_team_name)
	{
		$o_Result = $this->getO_mysqli();
		
		$s_query = "SELECT DISTINCT da_deadline, da_finished FROM tbl_dates".
				" JOIN tbl_aea_team ON tbl_dates.da_id_aea_team = tbl_aea_team.at_id".
				" JOIN tbl_team ON tbl_team.te_id = tbl_aea_team.at_id_team".
				" JOIN tbl_aea ON tbl_aea.aea_id = tbl_aea_team.at_id_aea".
				" WHERE tbl_aea.aea_nr = ?".
				" AND tbl_team.te_team = ?";
		
		$stmt = $o_Result->prepare($s_query);
		$stmt->bind_param('ss', $s_aea_nr, $s_team_name);
		
		$stmt->execute();
		
		$s_result = $stmt ->get_result();
		
		
		while ($a_dsatz = $s_result -> fetch_assoc()){
			
			$this->a_detail_data[] = $a_dsatz;
		}
	}
	
	private function get_comment_data($s_aea_nr, $s_team_name)
	{
		$o_Result = $this->getO_mysqli();
		
		$s_querycomment = "SELECT co_comment, co_todo FROM tbl_comment".
				" JOIN tbl_aea_team ON tbl_aea_team.at_id = tbl_comment.co_id_aea_team".
				" JOIN tbl_team ON tbl_team.te_id = tbl_aea_team.at_id_team".
				" JOIN tbl_aea ON tbl_aea.aea_id = tbl_aea_team.at_id_aea".
				" WHERE tbl_aea.aea_nr = ?".
				" AND tbl_team.te_team = ?";
		
		$stmt = $o_Result->prepare($s_querycomment);
		$stmt->bind_param('ss', $s_aea_nr, $s_team_name);
		
		$stmt->execute();
		
		$s_result = $stmt->get_result();
		
		while ($a_dsatz = $s_result -> fetch_assoc()){
		$this->a_comment_data[] = $a_dsatz;	
			
		}
	}
	
	private function get_mailreminder_data($s_aea_nr, $s_team_name)
	{
		$o_Result = $this->getO_mysqli();
		
		$s_querymail = "SELECT da_dateMail FROM tbl_dates".
				" JOIN tbl_aea_team ON tbl_dates.da_id_aea_team = tbl_aea_team.at_id".
				" JOIN tbl_team ON tbl_team.te_id = tbl_aea_team.at_id_team".
				" JOIN tbl_aea ON tbl_aea.aea_id = tbl_aea_team.at_id_aea".
				" WHERE tbl_aea.aea_nr = ?".
				" AND tbl_team.te_team = ?";
		
		$stmt = $o_Result->prepare($s_querymail);
		$stmt->bind_param('ss', $s_aea_nr, $s_team_name);
		
		$stmt->execute();
		
		$s_result = $stmt->get_result();
		
		while ($a_dsatz = $s_result -> fetch_assoc()){
		$this->a_mailreminder_data[] = $a_dsatz;
		
		}
	}
	
	/**
	 * @return multitype:
	 */
	public function getA_detail_data()
	{
		return $this->a_detail_data;
	}
	
	/**
	 * @return multitype:
	 */
	public function getA_comment_data()
	{
		return $this->a_comment_data;
	}
	
	/**
	 * @return multitype:
	 */
	public function getA_mailreminder_data()
	{
		return $this->a_mailreminder_data;
	}
	
}

