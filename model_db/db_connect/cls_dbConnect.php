<?php

namespace model_db\db_connect;

use Exception;
use php_classes\cls_infoServer;

class cls_dbConnect extends cls_infoServer {

	private $s_host;
	private $s_dataBase;
	private $s_dbUser;
	private $s_dbUserPw;
	private $o_mysqli;
	
	
	public function __construct($s_host,$s_dataBase,$s_dbUser,$s_dbUserPw)
	{
		parent::__construct();
		
		$this->s_host = $s_host;
		$this->s_dataBase = $s_dataBase;
		$this->s_dbUser = $s_dbUser;
		$this->s_dbUserPw = $s_dbUserPw;
		
		$this->f_connect();
		
	}
	
	private function f_connect()
	{
		error_reporting(0);
		try {
			$o_mysqli = new \mysqli($this->s_host, $this->s_dbUser, $this->s_dbUserPw, $this->s_dataBase);
			if ($o_mysqli->connect_errno){
				throw new Exception("dbConnnect");
			}
			$o_mysqli->set_charset("utf8mb4");
		}
		catch (Exception $e){
			echo "Fehler:".$this->getS_uriPre()."/ueb-aea/err/err.php?f=".$e->getMessage();
			exit;
		}
				
		$this->o_mysqli = $o_mysqli;
		
	}
	

	/**
	 * @return mixed
	 */
	public function getO_mysqli()
	{
		return $this->o_mysqli;
	}

}

