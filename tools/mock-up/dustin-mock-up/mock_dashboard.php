<?php

?>
<!doctype html>
<html lang="de" class="h-100">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<title>Dashboard</title>

<meta http-equiv="X-UA-Compatible" content="IE=10" /> 

<link href="<?php // echo $s_pathPre; ?>../../../library/bootstrap-4.5.2-dist/css/bootstrap.css" rel="stylesheet" >
<link href="<?php // echo $s_pathPre; ?>../../../library/bootstrap-4.5.2-dist/css/sticky-footer-navbar.css" rel="stylesheet">

<link rel="shortcut icon" type="image/x-icon" href="<?php //echo $s_pathPre; ?>../../../img/favicon-32x32-keiml.png">

<link rel="stylesheet" type="text/css" href="<?php //echo $s_pathPre; ?>../../../library/dataTables/css/jquery.dataTables.css"/>
<link href="<?php // echo $s_pathPre; ?>../../../css/own_design.css" rel="stylesheet">
<link href="<?php // echo $s_pathPre; ?>../../../css/site.css" rel="stylesheet">
</head>

<body class="d-flex flex-column h-100">
<header>
<!-- Fixed navbar -->
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
<a class="navbar-brand" href="#">SVLFG - Überwachung von Auslieferungsbeschreibungen</a>
</nav>
</header>
<main role="main" class="flex-shrink-0">
<div class="body_container">
<h1 class="mt-5 text-center">Dashboard</h1>
</div>
<div class="table_container">

<h2>Aktuell sind noch 3 (Platzhalter für Zähler) Anträge offen:</h2>
<br>
<div class="row text-center">
<div id="aaeNR">

<dl>
<dt><a class="Nummer" id="Nummer1" name="test" href="mock_antragsdaten.php"><b>8000008956</b></a>
<b>zu erledigen bis:</b>
<p class="Termin" href="mock_antragsdaten.php"><b>21.08.2020</b></p></dt>
<br>
<dt><a class="Nummer" href="mock_antragsdaten.php"><b>8000008957</b></a>
<b>zu erledigen bis:</b>
<p class="Termin" href="mock_antragsdaten.php"><b>17.08.2020</b></p></dt>
<br>
<dt><a class="Nummer" href="mock_antragsdaten.php"><b>8000008958</b></a>
<b>zu erledigen bis:</b>
<p class="Termin" href="mock_antragsdaten.php"><b>17.08.2020</b></p></dt>
</dl>

 				</div>
    		</div>
	  	</div>
<div id="gesamtliste">
<a class="button_special" id="liste_aeA" href="mock_gesamtliste_AAE.php"><b>Liste aller AÄ</b></a>
</div>	
<div id="neuanlage">
<a class="button_special" id="neuer_Eintrag" href="mock_neuanlage.php"><b>Neuer Eintrag</b></a>
</div>  	
   	 </main>
    <footer class="footer mt-auto py-3">
      <div class="container">
        <span class="text-container">SVLFG - Überwachung von Auslieferungsbeschreibungen</span>
      </div>
    </footer>
    
    
		
    <script src="<?php //echo $s_pathPre; ?>../../../library/jquery/jquery-3.5.1.min.js" ></script>
    <script src="<?php //echo $s_pathPre; ?>../../../library/bootstrap-4.5.2-dist/js/bootstrap.bundle.min.js"></script>
    <script src="<?php //echo $s_pathPre; ?>../../../library/dataTables/js/jquery.dataTables.min.js"></script>
    <!--  <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>-->
    <script src="../../../js/ajaxCall_dashboard.js"></script>
      
    </body>

</html>
