<?php
?>
<!doctype html>
<html lang="de" class="h-100">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<title>Neuanlage</title>
<meta http-equiv="X-UA-Compatible" content="IE=10" />
<link href="<?php // echo $s_pathPre; ?>../../../library/bootstrap-4.5.2-dist/css/bootstrap.css" rel="stylesheet" >
<link href="<?php // echo $s_pathPre; ?>../../../library/bootstrap-4.5.2-dist/css/sticky-footer-navbar.css" rel="stylesheet">
<link href="<?php // echo $s_pathPre; ?>../../../css/site.css" rel="stylesheet">
<link rel="shortcut icon" type="image/x-icon" href="<?php //echo $s_pathPre; ?>../../../img/favicon-32x32-keiml.png">

<link rel="stylesheet" type="text/css" href="<?php //echo $s_pathPre; ?>../../../library/dataTables/css/jquery.dataTables.css"/>

<link href="<?php // echo $s_pathPre; ?>../../../css/own_design.css" rel="stylesheet">
<link href="<?php // echo $s_pathPre; ?>../../../css/site.css" rel="stylesheet">


</head>
<body class="d-flex flex-column h-100">
<header>
<!-- Fixed navbar -->
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
<a class="navbar-brand" id="headline" href="#">SVLFG - Überwachung von Auslieferungsbeschreibungen</a>
<a class="navbar-brand" href="mock_dashboard.php">- Zurück zur Startseite</a>


</nav>
</header>
<main role="main" class="flex-shrink-0">
<div class="body_container">
<h1 class="mt-5 text-center">(Aufruf von Isabelles Modal)</h1>
</div>
<div class="table_container">
<table>

</table>
</div>
<div class="to_dashboard">
<a class="button_special" id="back_dashboard" href="mock_dashboard.php"><b>Zurück zur Startseite</b></a>

</div>
</main>
    <footer class="footer mt-auto py-3">
      <div class="container">
        <span class="text-container">SVLFG - Überwachung von Auslieferungsbeschreibungen</span>
      </div>
    </footer>
    
    
        
    <script src="<?php //echo $s_pathPre; ?>../../../library/jquery/jquery-3.5.1.min.js" ></script>
    <script src="<?php //echo $s_pathPre; ?>../../../library/bootstrap-4.5.2-dist/js/bootstrap.bundle.min.js"></script>
    <script src="<?php //echo $s_pathPre; ?>../../../library/dataTables/js/jquery.dataTables.min.js"></script>
    <!--  <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>-->
    <script src="../../../js/ajaxCall.js"></script>
      
    </body>
</html>

