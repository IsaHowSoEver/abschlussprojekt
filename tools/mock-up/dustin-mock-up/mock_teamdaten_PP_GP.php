<?php
?>
<!doctype html>
<html lang="de" class="h-100">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<title>Team-Daten</title>
<meta http-equiv="X-UA-Compatible" content="IE=10" />
<link href="<?php // echo $s_pathPre; ?>../../../library/bootstrap-4.5.2-dist/css/bootstrap.css" rel="stylesheet" >
<link href="<?php // echo $s_pathPre; ?>../../../library/bootstrap-4.5.2-dist/css/sticky-footer-navbar.css" rel="stylesheet">

<link rel="shortcut icon" type="image/x-icon" href="<?php //echo $s_pathPre; ?>../../../img/favicon-32x32-keiml.png">

<link rel="stylesheet" type="text/css" href="<?php //echo $s_pathPre; ?>../../../library/dataTables/css/jquery.dataTables.css"/>
<link href="<?php // echo $s_pathPre; ?>../../../css/own_DataTables.css" rel="stylesheet">

<link href="<?php // echo $s_pathPre; ?>../../../css/own_design.css" rel="stylesheet">
<link href="<?php // echo $s_pathPre; ?>../../../css/site.css" rel="stylesheet">


</head>
<body class="d-flex flex-column h-100">
<header>
<!-- Fixed navbar -->
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
<a class="navbar-brand" id="headline" href="#">SVLFG - Überwachung von Auslieferungsbeschreibungen</a>
<a class="navbar-brand" href="mock_dashboard.php">- Zurück zur Startseite</a>

</nav>
</header>
<main role="main" class="flex-shrink-0">
<div class="body_container">
<h1 class="mt-5 text-center">Team-Daten:</h1>
</div>
<div class="table_container">
<?php
$teamname = "5020404_PP_GP";
echo "Zum Team $teamname gehören die folgenden Anträge:";
?>
<p>Anderes Team auswählen:</p>
<div class="dropdown">
  <button class="btn-lg btn-success dropdown-toggle" type="button" data-toggle="dropdown">Team-Bezeichnung
  <span class="caret"></span></button>
  <ul class="dropdown-menu">
  
  <!-- Für den Mockup ist dieser Bereich statisch erstellt, in der produktiven Umgebung soll es dynamisch gefüllt werden. -->
    <li><a href="mock_teamdaten.php">5020104_Rollen</a></li>
    <li><a href="mock_teamdaten_Archiv.php">5020101_Archiv</a></li>
    <li><a href="mock_teamdaten_Buero.php">5020103_Buero</a></li>
    <li><a href="mock_teamdaten_FPS.php">50502_FPS</a></li>
    <li><a href="mock_teamdaten_PI.php">5020503_PI</a></li>
    <li><a href="mock_teamdaten_Portal_MS.php">5020501_Portal_MS</a></li>
    <li><a href="#">5020404_PP_GP</a></li>
    <li><a href="mock_teamdaten_UL_SRV.php">5030301_UL_SRV</a></li>
  </ul>
</div>
<p>(Email-Adresse vom Team)</p>
	
	<table>
	<tr>
		<th>Antragsnummer:</th>
    	<th>zu Erledigen bis:</th>
    	<th>Abgeschlossen am:</th>
	</tr>
	<tr>
		<td>8000008956</td>
		<td>(Beispiel-Datum)</td>
		<td>(Beispiel-Datum)</td>
	</tr>
	<tr>
		<td>8000008957</td>
		<td>(Beispiel-Datum)</td>
		<td>(Beispiel-Datum)</td>
	</tr>
	<tr>
		<td>8000008958</td>
		<td>(Beispiel-Datum)</td>
		<td>(Beispiel-Datum)</td>
	</tr>
	<tr>
		<td>8000008959</td>
		<td>(Beispiel-Datum)</td>
		<td>(Beispiel-Datum)</td>
	</tr>
	<tr>
		<td>8000009238</td>
		<td>(Beispiel-Datum)</td>
		<td>(Beispiel-Datum)</td>
	</tr>
	<tr>
		<td>8000009346</td>
		<td>(Beispiel-Datum)</td>
		<td>(Beispiel-Datum)</td>
	</tr>
</table>
</div>
<div class="comment_container">
<table>
	<tr>
		<th>Zu Antragsnummer</th>
		<th>Kommentar</th>
		<th>To Do</th>
	</tr>
	<tr>
		<td>8000008956</td>
		<td>Verzögerung bei der Bearbeitung werden erwartet</td>
		<td>(text)</td>
	</tr>
	<tr>
		<td>8000008957</td>
		<td>(Beispiel-Kommentar)</td>
		<td>(text)</td>
	</tr>
</table>
</div>
<div class="mail_container">
	<table>
	<thead>
		<tr>
			<th>Zu Antragsnummer</th>
			<th>Mail-Erinnerung gesendet am</th>
		</tr>
		<tr>
			<td>8000008956</td>
			<td>01.12.2020</td>
			
		</tr>
		<tr>
			<td>8000008957</td>
			<td>(Beispiel-Datum)</td>
			
		</tr>
	</thead>
	</table>

<div class="to_dashboard">
<a class="button_special" id="back_dashboard" href="mock_neuanlage.php"><b>Neues Team hinzufügen</b></a>
<a class="button_special" id="back_dashboard" href="mock_neuanlage.php"><b>Neuer Eintrag</b></a>

</div>
</main>
    <footer class="footer mt-auto py-3">
      <div class="container">
        <span class="text-container">SVLFG - Überwachung von Auslieferungsbeschreibungen</span>
      </div>
    </footer>
    
    <script src="<?php //echo $s_pathPre; ?>../../../library/jquery/jquery-3.5.1.min.js" ></script>
    <script src="<?php //echo $s_pathPre; ?>../../../library/bootstrap-4.5.2-dist/js/bootstrap.bundle.min.js"></script>
    <script src="<?php //echo $s_pathPre; ?>../../../library/dataTables/js/jquery.dataTables.min.js"></script>
    <!--  <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>-->
    <script src="../../../js/ajaxCall.js"></script>   
        
    </body>
</html>