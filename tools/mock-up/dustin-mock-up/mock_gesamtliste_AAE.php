<?php
?>
<!doctype html>
<html lang="de" class="h-100">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<title>Gesamtliste AÄ</title>
<meta http-equiv="X-UA-Compatible" content="IE=10" />
<link href="<?php // echo $s_pathPre; ?>../../../library/bootstrap-4.5.2-dist/css/bootstrap.css" rel="stylesheet" >
<link href="<?php // echo $s_pathPre; ?>../../../library/bootstrap-4.5.2-dist/css/sticky-footer-navbar.css" rel="stylesheet">

<link rel="shortcut icon" type="image/x-icon" href="<?php //echo $s_pathPre; ?>../../../img/favicon-32x32-keiml.png">

<link rel="stylesheet" type="text/css" href="<?php //echo $s_pathPre; ?>../../../library/dataTables/css/jquery.dataTables.css"/>
<link href="<?php // echo $s_pathPre; ?>../../../css/own_DataTables.css" rel="stylesheet">

<link href="<?php // echo $s_pathPre; ?>../../../css/own_design.css" rel="stylesheet">
<link href="<?php // echo $s_pathPre; ?>../../../css/site.css" rel="stylesheet">


</head>
<body class="d-flex flex-column h-100">
<header>
<!-- Fixed navbar -->
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
<a class="navbar-brand" id="headline" href="#">SVLFG - Überwachung von Auslieferungsbeschreibungen</a>
<a class="navbar-brand" href="mock_dashboard.php">- Zurück zur Startseite</a>

</nav>
</header>
<main role="main" class="flex-shrink-0">
<div class="body_container">
<h1 class="mt-5 text-center">Gesamtliste offene Änderunganträge</h1>
</div>
<div class="table_container">
	<table>
		<tr>
    		<th>AÄ-Nummer</th>
    		<th>Realisierungsteams</th>
    		<th>zu erledigen bis</th>
		</tr>
		<tr>
			<td><a class="Nummer" href="mock_antragsdaten.php">8000008956</a></td>
			<td><a class="Team" href="mock_teamdaten.php">5020104_Rollen</a>,
			<a class="Team" href="mock_teamdaten_Portal_MS.php">5020501_Portal_MS</a>,
			<a class="Team" href="mock_teamdaten_Archiv.php">5020101_Archiv</a>,
			<a class="Team" href="mock_teamdaten_PP_GP.php">5020404_PP_GP</a>,
			<a class="Team" href="mock_teamdaten_UL_SRV.php">5030301_UL_SRV</a></td>
			<td class="Termin">21.08.2020</td>
		</tr>
		<tr>
			<td><a class="Nummer" href="mock_antragsdaten.php">8000008957</a></td>
			<td><a class="Team" href="mock_teamdaten_PI.php">5020503_PI</a>,
			<a class="Team" href="mock_teamdaten_Archiv.php">5020101_Archiv</a>,
			<a class="Team" href="mock_teamdaten_Buero.php">5020103_Buero</a>,
			<a class="Team" href="mock_teamdaten_PP_GP.php">5020404_PP_GP</a>,
			<a class="Team" href="mock_teamdaten_FPS.php">50502_FPS</a></td>
			<td class="Termin">17.08.2020</td>
		</tr>
		<tr>
			<td><a class="Nummer" href="mock_antragsdaten.php">8000008958</a></td>
			<td><a class="Team" href="mock_teamdaten.php">5020104_Rollen</a>,
			<a class="Team" href="mock_teamdaten_PI.php">5020503_PI</a>,
			<a class="Team" href="mock_teamdaten_PP_GP.php">5020404_PP_GP</a>,
			<a class="Team" href="mock_teamdaten_FPS.php">50502_FPS</a></td>
			<td class="Termin">17.08.2020</td>
		</tr>
	</table>
</div>
<div class="to_dashboard"> <!-- Eventuell den Class Namen ändern --> 
<a class="button_special" id="back_dashboard" href="mock_dashboard.php"><b>Zurück zur Startseite</b></a>

</div>
</main>
    <footer class="footer mt-auto py-3">
      <div class="container">
        <span class="text-container">SVLFG - Überwachung von Auslieferungsbeschreibungen</span>
      </div>
    </footer>
    
    
        
    <script src="<?php //echo $s_pathPre; ?>../../../library/jquery/jquery-3.5.1.min.js" ></script>
    <script src="<?php //echo $s_pathPre; ?>../../../library/bootstrap-4.5.2-dist/js/bootstrap.bundle.min.js"></script>
    <script src="<?php //echo $s_pathPre; ?>../../../library/dataTables/js/jquery.dataTables.min.js"></script>
    <!--  <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>-->
    <script src="../../../js/ajaxCall.js"></script>
      
    </body>
</html>

