<?php 

use model_db\db_connect\cls_dbConnect;
use php_classes\cls_infoServer;

/* Autoload Klassen */
function clsAutoloader($class) {
	$path = "$class.php";
	$path = str_replace ( "\\", "/", $path );
	$path =  "../../".$path;
	
	if (file_exists ( $path )) {
		require $path;
	}
}
spl_autoload_register ( 'clsAutoloader' );



echo $s_basePath;

$o_server = new cls_infoServer();
$s_pathPre = $o_server->getS_uriPre().$o_server->getS_path();


//$o_db = new cls_dbConnect("a", "s", "d", "f");   // = Verbindungsfehler provozieren
?>
<!doctype html>
<html lang="de" class="h-100">
	  <head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="description" content="">
	    <title>SVLFG - ue-aea</title>
	  <meta http-equiv="X-UA-Compatible" content="IE=10" /> 
	   
		<link href="<?php echo $s_pathPre; ?>library/bootstrap-4.5.2-dist/css/bootstrap.css" rel="stylesheet" >
		<link href="<?php echo $s_pathPre; ?>library/bootstrap-4.5.2-dist/css/sticky-footer-navbar.css" rel="stylesheet">
		<link href="<?php echo $s_pathPre; ?>css/site.css" rel="stylesheet">
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $s_pathPre; ?>img/favicon-32x32-keiml.png">	
	  
	
	  </head>
      <body class="d-flex flex-column h-100">
       <header>
          <!-- Fixed navbar -->
          <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">SVLFG - Überwachung von Auslieferungsbeschreibungen</a>
           </nav>
       </header>
	<main role="main" class="flex-shrink-0">
		<div class="container ">
        	<h1 class="mt-5 text-center">SVLFG - Überwachung von Auslieferungsbeschreibungen</h1>
		</div>  
		<div class="container">
    		<div class="row text-center">
 					<?php echo $_SERVER["PHP_SELF"]; ?>
 					
    		</div>
	  	</div>
   	 </main>
    <footer class="footer mt-auto py-3">
      <div class="container">
        <span class="text-container">SVLFG - Überwachung von Auslieferungsbeschreibungen</span>
      </div>
    </footer>
    <script src="<?php echo $s_pathPre; ?>library/jquery/jquery-3.5.1.min.js" ></script>
    <script src="<?php echo $s_pathPre; ?>library/bootstrap-4.5.2-dist/js/bootstrap.bundle.min.js"></script>
      
    </body>

</html>