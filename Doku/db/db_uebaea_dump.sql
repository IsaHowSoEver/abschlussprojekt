-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 29. Mrz 2021 um 10:01
-- Server-Version: 10.1.34-MariaDB
-- PHP-Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `db_uebaea`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_aea`
--

CREATE TABLE `tbl_aea` (
  `aea_id` int(11) NOT NULL,
  `aea_nr` varchar(255) CHARACTER SET utf8 NOT NULL,
  `aea_link` varchar(255) CHARACTER SET utf8 NOT NULL,
  `aea_finish` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `tbl_aea`
--

INSERT INTO `tbl_aea` (`aea_id`, `aea_nr`, `aea_link`, `aea_finish`) VALUES
(1, '8000008956', '\\lsv.dedfsgdatProjektswueb_aus80000089568000008956_Auslieferungsbeschreibung.docm', 0),
(2, '8000008957', '\\lsv.dedfsgdatProjektswueb_aus80000089578000008957_Auslieferungsbeschreibung.docm', 0),
(3, '8000008958', '\\lsv.dedfsgdatProjektswueb_aus80000089588000008958_Auslieferungsbeschreibung.docm', 0),
(4, '8000008959', '\\lsv.dedfsgdatProjektswueb_aus80000089598000008959_Auslieferungsbeschreibung.docm', 0),
(5, '8000009346', '\\lsv.dedfsgdatProjektswueb_ausErledigt80000093468000009346_Auslieferungsbeschreibung.docm', 1),
(6, '8000009238', '\\lsv.dedfsgdatProjektswueb_ausErledigt80000092388000009238_Auslieferungsbeschreibung.docm', 1),
(7, '123456789', 'test', 0),
(8, '23071991', 'Link', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_aea_team`
--

CREATE TABLE `tbl_aea_team` (
  `at_id` int(11) NOT NULL,
  `at_id_aea` int(11) NOT NULL,
  `at_id_team` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `tbl_aea_team`
--

INSERT INTO `tbl_aea_team` (`at_id`, `at_id_aea`, `at_id_team`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 6),
(7, 2, 3),
(8, 2, 7),
(9, 2, 4),
(10, 2, 8),
(11, 3, 1),
(12, 3, 6),
(13, 3, 4),
(14, 3, 8),
(15, 4, 6),
(16, 4, 4),
(17, 4, 3),
(18, 4, 8),
(19, 5, 1),
(20, 5, 4),
(21, 5, 2),
(22, 6, 1),
(23, 6, 4),
(24, 6, 6);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_comment`
--

CREATE TABLE `tbl_comment` (
  `co_id` int(11) NOT NULL,
  `co_comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `co_todo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `co_id_aea_team` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_dates`
--

CREATE TABLE `tbl_dates` (
  `da_id` int(11) NOT NULL,
  `da_dateMail` date DEFAULT NULL,
  `da_deadline` date DEFAULT NULL,
  `da_id_aea_team` int(11) NOT NULL,
  `da_finished` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `tbl_dates`
--

INSERT INTO `tbl_dates` (`da_id`, `da_dateMail`, `da_deadline`, `da_id_aea_team`, `da_finished`) VALUES
(1, NULL, '2020-08-21', 1, '2020-07-01'),
(2, '2020-08-13', '2020-08-21', 2, '2020-08-21'),
(3, '2020-08-19', '2020-08-21', 2, '2020-08-21'),
(4, NULL, '2020-08-21', 3, '2020-06-30'),
(5, NULL, '2020-08-24', 4, '2020-06-30'),
(6, '2020-08-13', '2020-08-20', 5, '2020-08-20'),
(7, '2020-08-19', '2020-08-20', 5, '2020-08-20');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_team`
--

CREATE TABLE `tbl_team` (
  `te_id` int(11) NOT NULL,
  `te_team` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `te_mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `tbl_team`
--

INSERT INTO `tbl_team` (`te_id`, `te_team`, `te_mail`) VALUES
(1, '5020104_Rollen', '5020104_RWz_Leitung@svlfg.de\r\n'),
(2, '5020501_Portal_MS\r\n', '5020501_PORTAL_MS_PF@svlfg.de'),
(3, '5020101_Archiv', '5020101_Archiv_Leitung@svlfg.de'),
(4, '5020404_PP_GP', '5020404_Personendaten_PF@svlfg.de'),
(5, '5030301_UL_SRV', '5030301_UL_Srv@svlfg.de'),
(6, '5020503_PI', '5020503_PI_PF@svlfg.de'),
(7, '5020103_Buero', '5020103_Buero_Leitung@svlfg.de'),
(8, '50502_FPS', '50502_FPS_PF@svlfg.de'),
(10, 'Testteam2', 'team2@test.de');

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `testview`
-- (Siehe unten für die tatsächliche Ansicht)
--
CREATE TABLE `testview` (
`da_dateMail` date
,`da_deadline` date
);

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `view_dashboard`
-- (Siehe unten für die tatsächliche Ansicht)
--
CREATE TABLE `view_dashboard` (
`aea_id` int(11)
,`aea_nr` varchar(255)
,`da_deadline` date
);

-- --------------------------------------------------------

--
-- Struktur des Views `testview`
--
DROP TABLE IF EXISTS `testview`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `testview`  AS  select `tbl_dates`.`da_dateMail` AS `da_dateMail`,`tbl_dates`.`da_deadline` AS `da_deadline` from `tbl_dates` where (`tbl_dates`.`da_id_aea_team` = 2) ;

-- --------------------------------------------------------

--
-- Struktur des Views `view_dashboard`
--
DROP TABLE IF EXISTS `view_dashboard`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_dashboard`  AS  select `tbl_aea`.`aea_id` AS `aea_id`,`tbl_aea`.`aea_nr` AS `aea_nr`,`tbl_dates`.`da_deadline` AS `da_deadline` from (`tbl_aea` join `tbl_dates`) where ((`tbl_aea`.`aea_id` = `tbl_dates`.`da_id`) and (`tbl_aea`.`aea_finish` = 0)) ;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `tbl_aea`
--
ALTER TABLE `tbl_aea`
  ADD PRIMARY KEY (`aea_id`);

--
-- Indizes für die Tabelle `tbl_aea_team`
--
ALTER TABLE `tbl_aea_team`
  ADD PRIMARY KEY (`at_id`),
  ADD KEY `tbl_aea_team_ibfk_1` (`at_id_aea`),
  ADD KEY `tbl_aea_team_ibfk_2` (`at_id_team`);

--
-- Indizes für die Tabelle `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD PRIMARY KEY (`co_id`),
  ADD KEY `co_id_aea_team` (`co_id_aea_team`);

--
-- Indizes für die Tabelle `tbl_dates`
--
ALTER TABLE `tbl_dates`
  ADD PRIMARY KEY (`da_id`),
  ADD KEY `da_id_aea_team` (`da_id_aea_team`);

--
-- Indizes für die Tabelle `tbl_team`
--
ALTER TABLE `tbl_team`
  ADD PRIMARY KEY (`te_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `tbl_aea`
--
ALTER TABLE `tbl_aea`
  MODIFY `aea_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT für Tabelle `tbl_aea_team`
--
ALTER TABLE `tbl_aea_team`
  MODIFY `at_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT für Tabelle `tbl_comment`
--
ALTER TABLE `tbl_comment`
  MODIFY `co_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `tbl_dates`
--
ALTER TABLE `tbl_dates`
  MODIFY `da_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT für Tabelle `tbl_team`
--
ALTER TABLE `tbl_team`
  MODIFY `te_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `tbl_aea_team`
--
ALTER TABLE `tbl_aea_team`
  ADD CONSTRAINT `tbl_aea_team_ibfk_1` FOREIGN KEY (`at_id_aea`) REFERENCES `tbl_aea` (`aea_id`),
  ADD CONSTRAINT `tbl_aea_team_ibfk_2` FOREIGN KEY (`at_id_team`) REFERENCES `tbl_team` (`te_id`);

--
-- Constraints der Tabelle `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD CONSTRAINT `tbl_comment_ibfk_1` FOREIGN KEY (`co_id_aea_team`) REFERENCES `tbl_aea_team` (`at_id`);

--
-- Constraints der Tabelle `tbl_dates`
--
ALTER TABLE `tbl_dates`
  ADD CONSTRAINT `tbl_dates_ibfk_1` FOREIGN KEY (`da_id_aea_team`) REFERENCES `tbl_aea_team` (`at_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
