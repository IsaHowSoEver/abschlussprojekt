-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 26. Nov 2020 um 18:07
-- Server-Version: 10.1.34-MariaDB
-- PHP-Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `db_uebaea`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `tbl_aea`
--

CREATE TABLE `tbl_aea` (
  `aea_id` int(11) NOT NULL,
  `aea_nr` varchar(255) CHARACTER SET utf8 NOT NULL,
  `aea_link` varchar(255) CHARACTER SET utf8 NOT NULL,
  `aea_finish` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `tbl_aea_team`
--

CREATE TABLE `tbl_aea_team` (
  `at_id` int(11) NOT NULL,
  `at_id_aea` int(11) NOT NULL,
  `at_id_team` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `tbl_comment`
--

CREATE TABLE `tbl_comment` (
  `co_id` int(11) NOT NULL,
  `co_comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `co_todo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `co_id_aea_team` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `tbl_dates`
--

CREATE TABLE `tbl_dates` (
  `da_id` int(11) NOT NULL,
  `da_dateMail` date NOT NULL,
  `da_deadline` date NOT NULL,
  `da_id_aea_team` int(11) NOT NULL,
  `da_finished` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `tbl_team`
--

CREATE TABLE `tbl_team` (
  `te_id` int(11) NOT NULL,
  `te_team` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `te_mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


--
-- Indizes der exportierten Tabellen
--

--
-- Indizes f�r die Tabelle `tbl_aea`
--
ALTER TABLE `tbl_aea`
  ADD PRIMARY KEY (`aea_id`);

--
-- Indizes f�r die Tabelle `tbl_aea_team`
--
ALTER TABLE `tbl_aea_team`
  ADD PRIMARY KEY (`at_id`),
  ADD KEY `tbl_aea_team_ibfk_1` (`at_id_aea`),
  ADD KEY `tbl_aea_team_ibfk_2` (`at_id_team`);

--
-- Indizes f�r die Tabelle `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD PRIMARY KEY (`co_id`),
  ADD KEY `co_id_aea_team` (`co_id_aea_team`);

--
-- Indizes f�r die Tabelle `tbl_dates`
--
ALTER TABLE `tbl_dates`
  ADD PRIMARY KEY (`da_id`),
  ADD KEY `da_id_aea_team` (`da_id_aea_team`);

--
-- Indizes f�r die Tabelle `tbl_team`
--
ALTER TABLE `tbl_team`
  ADD PRIMARY KEY (`te_id`);

--
-- AUTO_INCREMENT f�r exportierte Tabellen
--

--
-- AUTO_INCREMENT f�r Tabelle `tbl_aea`
--
ALTER TABLE `tbl_aea`
  MODIFY `aea_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT f�r Tabelle `tbl_aea_team`
--
ALTER TABLE `tbl_aea_team`
  MODIFY `at_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT f�r Tabelle `tbl_comment`
--
ALTER TABLE `tbl_comment`
  MODIFY `co_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT f�r Tabelle `tbl_dates`
--
ALTER TABLE `tbl_dates`
  MODIFY `da_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT f�r Tabelle `tbl_team`
--
ALTER TABLE `tbl_team`
  MODIFY `te_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `tbl_aea_team`
--
ALTER TABLE `tbl_aea_team`
  ADD CONSTRAINT `tbl_aea_team_ibfk_1` FOREIGN KEY (`at_id_aea`) REFERENCES `tbl_aea` (`aea_id`),
  ADD CONSTRAINT `tbl_aea_team_ibfk_2` FOREIGN KEY (`at_id_team`) REFERENCES `tbl_team` (`te_id`);

--
-- Constraints der Tabelle `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD CONSTRAINT `tbl_comment_ibfk_1` FOREIGN KEY (`co_id_aea_team`) REFERENCES `tbl_aea_team` (`at_id`);

--
-- Constraints der Tabelle `tbl_dates`
--
ALTER TABLE `tbl_dates`
  ADD CONSTRAINT `tbl_dates_ibfk_1` FOREIGN KEY (`da_id_aea_team`) REFERENCES `tbl_aea_team` (`at_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;